import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3
} from "native-base";
import { Avatar, ListItem } from 'react-native-elements';
import { inject, observer } from 'mobx-react'
import st from './style'


class FilterButton extends Component {
  render() {
    const { navigation, id, sender, subject, ago, distance, priceRange } = this.props
    return (
      <TouchableOpacity
        onPress={this.props.FilterAction} 
        style={st.Filter_Button}
      >
        <Text style={this.props.active ? st.Filter_Active_txt : st.Filter_Inactive_txt}>
          {this.props.text}
        </Text>
        
      </TouchableOpacity>
    )
  }
}

export default inject('authStore')(observer(FilterButton));
