import { StyleSheet } from 'react-native';
import {
  Colors,
  Fonts,
  Theme,
  //width, height
} from 'petapp/src/styles'


export default style = StyleSheet.create({
  Filter_Button: {
    backgroundColor:'white',
    borderRadius:20,
    paddingHorizontal:10,
    paddingVertical:5,
    marginRight: 10
  },
  Filter_Active_txt: {
    ...Theme.sublabel_txt,
    color: Colors.txtBlack,
    fontWeight: 'bold',
    fontSize:12
  },
  Filter_Inactive_txt: {
    ...Theme.sublabel_txt,
    color: 'gray',
    fontWeight: 'bold',
    fontSize:12
  },
});