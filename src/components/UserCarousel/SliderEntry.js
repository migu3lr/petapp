import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import styles from './SliderEntryStyle';
import { Colors } from './style';

export default class SliderEntry extends Component {
  
  render() {
    const { data: { title, subtitle }, even } = this.props;

    return (
      <TouchableOpacity
        activeOpacity={1}
        style={styles.slideInnerContainer}
      >
        <View style={styles.shadow} />
        <View style={styles.textContainer}>
          <Text style={styles.title}>
            $ 0
          </Text>
          <TouchableOpacity 
            style={styles.button}>
            <Icon
              name='wallet'
              color={'white'}
              size={20}
            />
            <Text style={{ fontWeight: 'bold', fontSize: 12, color:'white',marginHorizontal:10 }}>Detalles de mi saldo</Text>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  }
}