import React, { Component } from 'react';
import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3,
  List, ListItem
} from "native-base";
import Carousel, { Pagination } from 'react-native-snap-carousel';
import SliderEntry from './SliderEntry';
import { ENTRIES1 } from './entries';
import { sliderWidth, itemWidth } from './SliderEntryStyle';
import styles, { Colors } from './style';
import { Avatar, Icon } from 'react-native-elements';
import { inject, observer } from 'mobx-react'

const SLIDER_1_FIRST_ITEM = 0;

class UserCarousel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slider1ActiveSlide: SLIDER_1_FIRST_ITEM
    };
  }

  _renderItem({ item, index }) {
    return <SliderEntry data={item} even={(index + 1) % 2 === 0} />;
  }

  render() {
    const { slider1ActiveSlide } = this.state;

    return (
      <View style={styles.caroulselContainer}>
        <Carousel
          ref={c => this._slider1Ref = c}
          data={ENTRIES1}
          renderItem={this._renderItem}
          sliderWidth={sliderWidth}
          itemWidth={itemWidth}
          firstItem={SLIDER_1_FIRST_ITEM}
          inactiveSlideScale={0.94}
          inactiveSlideOpacity={0.5}
          containerCustomStyle={styles.slider}
          loopClonesPerSide={1}
          onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index })}
        />
        <Pagination
          dotsLength={ENTRIES1.length}
          activeDotIndex={slider1ActiveSlide}
          containerStyle={styles.paginationContainer}
          dotColor={'rgba(255, 255, 255, 0.92)'}
          dotStyle={styles.paginationDot}
          inactiveDotColor={Colors.cardIcon}
          inactiveDotOpacity={0.6}
          inactiveDotScale={0.6}
          carouselRef={this._slider1Ref}
        />
      </View>
    );
  }
}

export default inject('authStore')(observer(UserCarousel));

