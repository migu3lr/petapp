import { StyleSheet } from 'react-native';

export const Colors = {
    black: '#1a1917',
    gray: '#888888',
    background1: '#B721FF',
    background2: '#21D4FD',
    card: '#737C8E',
    cardButton: '#2C343A',
    cardIcon: '#7C7C7C'
};

const titleFont = 'Calibre-Semibold'

export default StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: Colors.black
    },
    container: {
        flex: 1,
        backgroundColor: Colors.background1
    },
    gradient: {
        ...StyleSheet.absoluteFillObject
    },
    scrollview: {
        flex: 1
    },
    caroulselContainer: {
        paddingVertical: 15
    },
    title: {
        paddingHorizontal: 30,
        backgroundColor: 'transparent',
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        fontFamily: titleFont,
    },
    titleDark: {
        color: Colors.black
    },
    subtitle: {
        marginTop: 5,
        paddingHorizontal: 30,
        backgroundColor: 'transparent',
        color: 'rgba(255, 255, 255, 0.75)',
        fontSize: 13,
        fontStyle: 'italic',
        textAlign: 'center'
    },
    slider: {
        //marginTop: 15,
        overflow: 'visible' // for custom animations
    },
    sliderContentContainer: {
        paddingVertical: 10 // for custom animation
    },
    paginationContainer: {
        paddingVertical: 8
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        //marginHorizontal: 8
    }
});