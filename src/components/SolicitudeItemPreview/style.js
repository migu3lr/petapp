import { StyleSheet } from 'react-native';
import {
  Colors,
  Fonts,
  Theme,
  //width, height
} from 'petapp/src/styles'


export default style = StyleSheet.create({
  subtitleView: {
    //paddingLeft: 10,
    //paddingTop: 5
  },
  rightTitleText: {
    ...Theme.sublabel_txt,
    color: Colors.txtBlack,
    fontSize:10
  },
  subtitleText: {
    ...Theme.sublabel_txt,
    color: Colors.txtBlack,
    fontSize:12
  },
  titleText: {
    ...Theme.sublabel_txt,
    color: Colors.txtBlack,
    fontWeight: 'bold',
    fontSize:12
  },
  WalkersView: {
    flexDirection:'row'
  },
  WalkerAvatar: {
    marginRight: 10
  }
});