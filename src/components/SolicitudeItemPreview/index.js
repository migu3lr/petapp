import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3
} from "native-base";
import { Avatar, ListItem } from 'react-native-elements';
import { inject, observer } from 'mobx-react'
import st from './style'


class SolicitudeItemPreview extends Component {
  render() {
    const { navigation, id, title, description, ago, status, appStore} = this.props
    const Dealers = appStore.DealsbyWork(id)

    return (
      <TouchableOpacity
        onPress={() => {
          switch (status) {
            case 'PENDING':
            case 'CONFIRMED':
              navigation.navigate('ViewSolicitude', { id })
              break;
            case 'IN_PROGRESS':
              navigation.navigate('ViewActivity', { id })
              break;
            default:
              navigation.navigate('ViewSolicitude', { id })
              break;
          }
          
        }}
      >
        <ListItem
          containerStyle={{ backgroundColor: 'rgba(255, 255, 255, 0)' }}
          title={title}
          titleStyle={st.titleText}
          titleProps={{ numberOfLines:1 }}
          subtitle={
            <View>
              <View style={st.subtitleView}>
                <Text style={st.subtitleText} numberOfLines={1}>{description}</Text>
                <Text style={st.subtitleText}>{status}</Text>
              </View>
              <View style={st.WalkersView}>
                {Dealers.map(
                  (deal, index) => (
                    <Avatar
                      key={index}
                      size='small'
                      rounded
                      containerStyle = {st.WalkerAvatar}
                      source={require('petapp/src/images/customer.jpg')}
                      onPress={() => navigation.navigate('Chat', { 
                        chatType: (status == 'IN_PROGRESS' ? 'work' : 'deal'), 
                        id:deal.id, 
                        date:deal.workId.createdat, 
                        deal
                      })}
                    />
                  )
                )}
              </View>
            </View>
          }
          rightTitle={<Text style={st.rightTitleText}>{ago}</Text>}
          bottomDivider
          
        />
      </TouchableOpacity>
    )
  }
}

export default inject('appStore','iotStore')(observer(SolicitudeItemPreview));
