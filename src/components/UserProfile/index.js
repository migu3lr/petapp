import React, { Component } from 'react';
import { StatusBar, Image, TouchableOpacity, KeyboardAvoidingView, ScrollView, TouchableHighlight } from 'react-native';
import { SwitchActions } from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';

import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3,
  List, ListItem
} from "native-base";
import { Input } from 'react-native-elements';
import OptionsMenu from 'react-native-options-menu';
import Icon from 'react-native-vector-icons/AntDesign'
import AsyncStorage from '@react-native-community/async-storage';
import { inject, observer } from 'mobx-react'

import { Theme, Colors } from 'petapp/src/styles/'
import st from './style'
import UserInformation from 'petapp/src/components/UserInformation'
import UserCarousel from 'petapp/src/components/UserCarousel'
import ContentPaper from 'petapp/src/components/ContentPaper'
import SectionTitle from 'petapp/src/components/SectionTitle'
import DealItemPreview from 'petapp/src/components/DealItemPreview'
import FilterButton from 'petapp/src/components/FilterButton'

import * as dtf from 'petapp/src/lib/datetime-format'

class UserProfileScreen extends Component {
  
  render() {
    const { appStore, authStore, navigation } = this.props

    const walker = navigation.getParam('walker', {});
    
    return (
      <Container>
        <StatusBar backgroundColor={Colors.bgColor} barStyle="light-content" />
        <Content contentContainerStyle={Theme.Content_view}>

          
          <ContentPaper
            title='Perfil de Usuario'
          >

            <UserInformation name={walker.full_name} starts={walker.prom_rate} />

            
          </ContentPaper>

        </Content>
      </Container >
    )
  }
}

export default inject('appStore', 'authStore', 'vOpts')(observer(UserProfileScreen));

