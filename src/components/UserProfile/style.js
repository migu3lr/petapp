import { StyleSheet } from 'react-native';
import {
  Colors,
  Fonts,
  Theme,
  //width, height
} from 'petapp/src/styles'


export default style = StyleSheet.create({
  FilterSection: {
    flexDirection: 'row',
    marginTop: 10
  },
  ChatContainerView: {
    //borderRadius: 30,
    borderTopLeftRadius:30,
    borderTopRightRadius: 30,
    padding: 15,
    backgroundColor: Colors.paperColor,    
    overflow:'hidden',
    flex:1
  },
  HeaderInfoView:{

  },
  HeaderStatusView:{
    borderWidth: 0.5,
    borderColor: Colors.txtBlack,
    marginVertical: 3
  },
  headerStatus: {
    ...Theme.sublabel_txt,
    color: Colors.txtBlack,
    fontWeight: 'bold',
    alignSelf: 'center',
    fontSize: Fonts.regularSize,
  },
  headerTitle: {
    ...Theme.sublabel_txt,
    color: Colors.txtBlack,
  },
  headerPrice: {
    ...Theme.sublabel_txt,
    color: Colors.txtBlack,
    fontWeight: 'bold',
    alignSelf: 'flex-end'
  },
  headerSubtitle: {
    ...Theme.content_txt,
    color: Colors.txtBlack,
    fontSize: Fonts.regularSize,
  },
  receivedMsg:{
    backgroundColor:'white',
    borderRadius:10,
    borderTopLeftRadius:0,
    paddingHorizontal: 8,
    paddingVertical:3,
    marginBottom: 5,
    alignSelf: 'flex-start',
    elevation: 1,
  },
  sentMsg:{
    backgroundColor: Colors.iconColor + '66',
    borderRadius:10,
    borderTopRightRadius: 0,
    paddingHorizontal: 8,
    paddingVertical:3,
    marginBottom: 5,
    alignSelf: 'flex-end',
    elevation: 1,
  },
  messageSender: {
    ...Theme.content_txt,
    fontSize: Fonts.regularSize,
    color: Colors.iconColor,
    fontWeight: 'bold',
    marginBottom:5
  },
  messageTxt: {
    ...Theme.content_txt,
    fontSize: Fonts.regularSize,
    color: Colors.txtBlack,
  },
  messageDate: {
    ...Theme.content_txt,
    fontSize: Fonts.litleSize,
    alignSelf: 'flex-end',
    color: Colors.cardIcon,
  }
});