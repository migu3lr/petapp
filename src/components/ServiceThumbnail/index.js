import React, { Component } from 'react';
import { StatusBar, Image, TouchableOpacity, KeyboardAvoidingView, ScrollView, TouchableHighlight } from 'react-native';
import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3,
  List, ListItem
} from "native-base";
import { Avatar, Icon } from 'react-native-elements';
import { inject, observer } from 'mobx-react'
import st from './style'


class ServiceThumbnail extends Component {
  render() {
    const { action, thumb, name } = this.props
    
    return (
      <TouchableOpacity style={st.container} onPress={()=>action()}>
        <Avatar
          size='medium'
          rounded
          source={thumb}
        />
        <Text style={st.title}>{name}</Text>
      </TouchableOpacity>
    )
  }
}

export default ServiceThumbnail;