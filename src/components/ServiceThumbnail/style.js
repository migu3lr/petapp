import { StyleSheet } from 'react-native';
import {
  Colors,
  Fonts,
  Theme,
  //width, height
} from 'petapp/src/styles'


export default style = StyleSheet.create({
  container: {
    marginHorizontal: 15,
    marginVertical: 10,
    alignItems: 'center',
  },
  title: {
    ...Theme.sublabel_txt,
    fontSize: Fonts.sublabelSize,
    textAlign: 'center',
  }
});