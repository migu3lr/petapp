import { StyleSheet } from 'react-native';
import {
  Colors,
  Fonts,
  Theme,
  //width, height
} from 'petapp/src/styles'


export default style = StyleSheet.create({
  title: {
    ...Theme.label_txt,
    fontSize: Fonts.contentSize,
    paddingVertical: 15, 
    textAlign: 'center'   
  }
});