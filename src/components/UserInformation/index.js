import React, { Component } from 'react';
import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3,
  List, ListItem
} from "native-base";
import { Avatar, Icon, Rating, AirbnbRating } from 'react-native-elements';
import { inject, observer } from 'mobx-react'
import st from './style'


class UserInformation extends Component {
  render() {

    return (
      <View style={st.container}>
        <Avatar
          size='medium'
          rounded
          source={require('petapp/src/images/customer.jpg')}
        />
        <View style={st.desc_view}>
          <Text style={st.name}>{this.props.name}</Text>
          {this.props.starts &&
            <View style={st.category_view}>
              <Rating imageSize={20} readonly fractions="{1}" startingValue={this.props.starts} />
            
            </View>
          }
        </View>

      </View>
    )
  }
}

export default inject('authStore')(observer(UserInformation));