import { StyleSheet } from 'react-native';
import {
  Colors,
  Fonts,
  Theme,
  //width, height
} from 'petapp/src/styles'


export default style = StyleSheet.create({
  container: {
    flexDirection: 'row', 
    paddingVertical: 15,
    marginHorizontal: 20
  },
  desc_view: {
    marginLeft: 10
  },
  name: {
    ...Theme.label_txt,
    fontSize: Fonts.contentSize,
  },
  category_view: {
    flexDirection: 'row', 
    alignItems: 'flex-end'
  },
  category: {
    ...Theme.sublabel_txt,
    fontSize: Fonts.sublabelSize,
    marginLeft: 5
  },
  category_icon: {
    fontSize: Fonts.sublabelSize,
    color: Colors.iconColor
  }
});