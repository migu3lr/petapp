import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3
} from "native-base";
import { Avatar, ListItem } from 'react-native-elements';
import { inject, observer } from 'mobx-react'
import st from './style'


class InboxItemPreview extends Component {
  render() {
    const { navigation, id, sender, subject, ago, distance, priceRange, status } = this.props

    return (
      <TouchableOpacity
        onPress={() => navigation.navigate('ViewWork', { id })}
      >
        <ListItem
          containerStyle={{ backgroundColor: 'rgba(255, 255, 255, 0)' }}
          title={subject}
          titleStyle={st.titleText}
          titleProps={{ numberOfLines:1 }}
          subtitle={
            <View style={st.subtitleView}>
              <Text style={st.subtitleText} numberOfLines={1}>{sender}</Text>
              <Text style={st.subtitleText}>{priceRange}</Text>
              <Text style={st.subtitleText}>{status}</Text>
            </View>
          }
          rightTitle={<Text style={st.rightTitleText}>{ago}</Text>}
          rightSubtitle={<Text style={st.rightTitleText}>{distance}</Text>}
          leftAvatar={{ source: require('petapp/src/images/customer.jpg') }}
          bottomDivider
        />
      </TouchableOpacity>
    )
  }
}

export default inject('authStore')(observer(InboxItemPreview));
