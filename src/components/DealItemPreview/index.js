import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3
} from "native-base";
import { Avatar, ListItem } from 'react-native-elements';
import { inject, observer } from 'mobx-react'
import st from './style'


class DealItemPreview extends Component {
  render() {
    const { navigation, id, customerName, lastMsgTime, lastMsgText, date } = this.props

    return (
      <TouchableOpacity
        onPress={() => navigation.navigate('Chat', { chatType:'deal', id, customerName, date,
          prueba: () => console.log('hola')
        })}
      >
        <ListItem
          containerStyle={{ backgroundColor: 'rgba(255, 255, 255, 0)' }}
          title={customerName}
          titleStyle={st.titleText}
          titleProps={{ numberOfLines:1 }}
          subtitle={
            <View style={st.subtitleView}>
              <Text style={st.subtitleText} numberOfLines={2}>{lastMsgText}</Text>
            </View>
          }
          rightTitle={<Text style={st.rightTitleText}>{lastMsgTime}</Text>}
          leftAvatar={{ source: require('petapp/src/images/customer.jpg') }}
          bottomDivider
        />
      </TouchableOpacity>
    )
  }
}

export default inject('authStore')(observer(DealItemPreview));
