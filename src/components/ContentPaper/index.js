import React, { Component } from 'react';
import { ScrollView } from 'react-native'
import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3,
  List, ListItem
} from "native-base";
import { Avatar, Icon } from 'react-native-elements';
import { inject, observer } from 'mobx-react'
import st from './style'


class ContentPaper extends Component {
  render() {
    return (
      <View style={st.container}>
        <View style={st.Header_View}>
          <View>
            <Text style={st.Title_txt}>{this.props.title}</Text>
            <Text style={st.Subtitle_txt}>{this.props.subtitle}</Text>
          </View>
          {this.props.options &&
            <View style={st.OptionsMenu_View}>
              {this.props.options}
            </View>
          }
        </View>

        {this.props.children}
        
      </View>
    )
  }
}

export default inject('authStore')(observer(ContentPaper));
