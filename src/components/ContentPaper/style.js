import { StyleSheet } from 'react-native';
import {
  Colors,
  Fonts,
  Theme,
  //width, height
} from 'petapp/src/styles'


export default style = StyleSheet.create({
  container: {
    //marginHorizontal: 20,
    marginTop: 15,
    //borderRadius: 30,
    borderTopLeftRadius:30,
    borderTopRightRadius: 30,
    padding: 30,
    backgroundColor: Colors.paperColor,    
    overflow:'hidden',
    flex:1
  },
  Title_txt: {
    ...Theme.label_txt,
    fontSize: Fonts.contentSize,
    color: Colors.txtBlack
  },
  Subtitle_txt: {
    ...Theme.sublabel_txt,
    color: Colors.txtBlack
  },
  Header_View: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  OptionsMenu_View: {
  },
});