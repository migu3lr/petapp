import React, { Component } from 'react';
import { StatusBar, Image, TouchableOpacity, KeyboardAvoidingView, ScrollView, TouchableHighlight } from 'react-native';
import { SwitchActions } from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';

import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3,
  List, ListItem
} from "native-base";
import { Input } from 'react-native-elements';
import OptionsMenu from 'react-native-options-menu';
import Icon from 'react-native-vector-icons/AntDesign'
import AsyncStorage from '@react-native-community/async-storage';
import { inject, observer } from 'mobx-react'

import { Theme, Colors } from 'petapp/src/styles/'
import st from './style'
import UserInformation from 'petapp/src/components/UserInformation'
import UserCarousel from 'petapp/src/components/UserCarousel'
import ContentPaper from 'petapp/src/components/ContentPaper'
import SectionTitle from 'petapp/src/components/SectionTitle'
import DealItemPreview from 'petapp/src/components/DealItemPreview'
import FilterButton from 'petapp/src/components/FilterButton'

import * as dtf from 'petapp/src/lib/datetime-format'

class ChatMessage extends Component {
  render() {
    const { sender, type, text, date } = this.props; 
    return (
      <View style={type=='sent' ? st.sentMsg : st.receivedMsg}>
        {type=='received' && <Text style={st.messageSender}>{sender}</Text>}
        <Text style={st.messageTxt}>{`${text}`}</Text>
        <Text style={st.messageDate}>{`${dtf.chatFormat(date)}`}</Text>
      </View>
    )
  }
}

class ChatScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      newMessage: '',
    }
  }

  render() {
    const { appStore, authStore, navigation } = this.props

    const id = navigation.getParam('id', '');
    const date = new Date(navigation.getParam('date', ''));
    const chatType = navigation.getParam('chatType', '');
    const chat = appStore.ChatbyIdType(id, chatType);
    
    const profile = authStore.user.profile
    const userId = authStore.user.identityId

    const input = React.createRef();

    
    return (
      <Container>
        <StatusBar backgroundColor={Colors.bgColor} barStyle="light-content" />
        <Content contentContainerStyle={Theme.Content_view}>

          
          <View style={st.ChatContainerView}>

            { chatType == 'deal' &&
              <View style={st.HeaderInfoView}>
                <View style={{ flexDirection:'row', justifyContent: 'space-between'}}>
                  <Text style={st.headerTitle}>{chat[0].workId.work_type}</Text>
                  <Text style={st.headerPrice}>{`$ ${chat[0].dealId.price.toLocaleString()}`}</Text>
                </View>
                <Text style={st.headerSubtitle}>{`Desde el ${dtf.dealTitleFormat(chat[0].workId.createdat)}, para un ${chat[0].workId.service_type}`}</Text>
                <View style={st.HeaderStatusView}>
                  <Text style={st.headerStatus}>{chat[0].dealId.status}</Text>
                </View>
              </View>
            }

            <ScrollView
              ref={ref => this.scrollView = ref}
              onContentSizeChange={(contentWidth, contentHeight)=>{        
                  this.scrollView.scrollToEnd({animated: true});
              }}
            >
              <ChatMessage sender={chat[0].workId.customer.full_name} type={profile == 'customer' ? 'sent' : 'received'} date={date}
                text={`ID Servicio: ${chat[0].workId.id}\n`
                  +`Fecha requerida del servicio: ${dtf.workFormat(chat[0].workId.createdat)}\n`
                  +`Ubicación: ${chat[0].workId.location.address}\n`
                  +`Nombre de la mascota: ${chat[0].workId.pet.name}\n`
                  +`Tipo de mascota: ${chat[0].workId.pet.type}\n`
                  +`Tamaño de la mascota: ${chat[0].workId.pet.size}\n`
                  +`Edad de la mascota: ${chat[0].workId.pet.age}\n`
                  +`Raza de la mascota: ${chat[0].workId.pet.breed}`} 
              />

              {chat.map(
                (msg, index) => {
                  let msgType = 'sent'
                  if(profile == msg.senderProfile) msgType = 'sent'
                  if(profile == msg.receiverProfile) msgType = 'received'

                  return <ChatMessage key={index} sender={msg.sender_name} type={msgType} text={msg.msgText} date={msg.sentAt}/>
                }
              )}
            </ScrollView>

            <View style={st.NewMessageView}>
              <Input 
                ref={input}
                inputContainerStyle={{
                  height: 35,
                  borderRadius: 10,
                  paddingHorizontal: 10,
                  backgroundColor: 'rgba(255,255,255, 0.5)',
                }}
                inputStyle={{
                  fontSize: 14,
                }}
                rightIcon={
                  <Icon 
                    name='rightcircleo'
                    size={24}
                    color='black'
                    onPress={() => {
                      const newMessage = this.state.newMessage
                      if (newMessage == '') input.current.shake();
                      else {
                        appStore.sendChatMessage(newMessage, chatType, chat[0].workId, chat[0].dealId )
                        input.current.clear();
                        this.setState({newMessage: ''})
                      }                        
                    }}
                  />
                }
                placeholder={'Escribe un mensaje'}
                onChangeText={newMessage => this.setState({newMessage})}
              />
            </View>
          </View>

        </Content>
      </Container >
    )
  }
}

export default inject('appStore', 'authStore', 'vOpts')(observer(ChatScreen));

