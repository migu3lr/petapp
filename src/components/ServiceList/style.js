import { StyleSheet } from 'react-native';
import {
  Colors,
  Fonts,
  Theme,
  //width, height
} from 'petapp/src/styles'


export default style = StyleSheet.create({
  container: {
    //flex:1,
    //alignContent: 'space-around',
    flexWrap: 'wrap',
    flexDirection:'row',
    justifyContent:'space-around', 
  }
});