import React, { Component } from 'react';
import { StatusBar, Image, TouchableOpacity, KeyboardAvoidingView, ScrollView, TouchableHighlight } from 'react-native';
import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3,
  List, ListItem
} from "native-base";
import { Avatar, Icon } from 'react-native-elements';
import { inject, observer } from 'mobx-react'
import st from './style'
import ServiceThumbnail from 'petapp/src/components/ServiceThumbnail'
import Services from 'petapp/src/stores/Services'

class ServiceList extends Component {
  render() {
    return (
      <ScrollView horizontal={true}>
        <View style={st.container}>
          {Services.map(
            (service, index) => (
              <ServiceThumbnail
                key={index}
                id={index}
                name={service.name}
                thumb={service.thumb}
                action={() =>service.action(this.props.customProps)}
              />
            )
          )}
        </View>
      </ScrollView>
    )
  }
}

export default ServiceList;
