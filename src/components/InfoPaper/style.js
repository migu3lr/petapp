import { StyleSheet } from 'react-native';
import {
  Colors,
  Fonts,
  Theme,
  //width, height
} from 'petapp/src/styles'


export default style = StyleSheet.create({
  container: {
    marginHorizontal: 20,
    marginVertical: 15,
    borderRadius: 30,
    padding: 5,
    backgroundColor: Colors.paperColor,    
    overflow:'hidden'
  }
});