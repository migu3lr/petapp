import React, { Component } from 'react';
import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3,
  List, ListItem
} from "native-base";
import { Avatar, Icon } from 'react-native-elements';
import { inject, observer } from 'mobx-react'
import st from './style'


class InfoPaper extends Component {
  render() {
    return (
      <View style={st.container}>
        {this.props.children}
      </View>
    )
  }
}

export default inject('authStore')(observer(InfoPaper));
