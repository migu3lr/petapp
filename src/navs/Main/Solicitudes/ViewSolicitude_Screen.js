import React, {Component} from 'react';
import { 
  Image, 
  View, 
  Text,
  TextInput, 
  ImageBackground, 
  StyleSheet, 
  Dimensions, 
  TouchableOpacity,
  ScrollView } from 'react-native';
import { 
  Container, 
  Button,
  Content,
  Card,
  CardItem,
  Icon,
  Title, 
  Footer, 
  List, ListItem,
  Left, 
  Right,
  Body } from 'native-base';
import { Avatar,Badge } from 'react-native-elements';
import IconAntD from 'react-native-vector-icons/AntDesign'

import * as dtf from 'petapp/src/lib/datetime-format'

import { inject, observer } from 'mobx-react'

import { Theme,Colors } from 'petapp/src/styles/'
import st from './style'


class WorkAttribute extends Component {
  render() {
    const { title, desc } = this.props
    if (desc) { 
      return (
        <View style={st.WorkAttribute_view}>
          <Text style={st.TitleText}>{title} </Text>
          <View style={st.WorkAttributeDesc_view}>
              <Icon style={st.WorkAttributeDesc_icon} name="arrow-forward" />
              <Text>{desc}</Text> 
          </View>
        </View>
      )
    }
    return null
  }
}

class WalkerDealer extends Component {
  render() {
    const { navigation, img, name, status, id, date, deal} = this.props
    
    return (
      <TouchableOpacity
        style={st.WalkerTouchable}
        onPress={() => navigation.navigate('Chat', { chatType:'deal', id, date, deal})}
      >
        <View>
          <Avatar
            size='large'
            rounded
            source={{uri: img}}
          />
          <Badge
            status="error"
            value="10"
            containerStyle={{ position: 'absolute', top: 2, right: 2 }}
          />
        </View>
        <Text style={st.WalkerDesc_text}>{name}</Text>
        <Text style={st.WalkerDesc_text}>{status}</Text>
      </TouchableOpacity>
    )
  }
}

class ViewSolicitudeScreen extends Component {
  render() {
    const { appStore, forms, navigation } = this.props

    const id = navigation.getParam('id', '');

    const work = appStore.WorkbyID(id);
    const Dealers = appStore.DealsbyWork(id)

    if(work == undefined) {
      return (
      
        <Container>
          <Content padder contentContainerStyle={Theme.Content_view}>
            <ScrollView>
              <Card>
                <CardItem header bordered>
                  <View>
                    <Text style={st.TitleText}>ERROR al buscar Solicitud.</Text>
                  </View>
                </CardItem>                
              </Card>
            </ScrollView>
          </Content>  
        </Container>
        
      )
    }

    
    return (
      
      <Container>
        <Content padder contentContainerStyle={Theme.Content_view}>
          <ScrollView>
            <Card>
              <CardItem>
                <Text style={st.TitleText}>Estos son los cuidadores que quieren atender a tu mastoca:</Text>
              </CardItem>
              <CardItem bordered>
                <Body>
                  
                  <ScrollView horizontal={true}>
                    <View style={st.WalkersView}>
                      {Dealers.map(
                        (deal, index) => (
                          <WalkerDealer
                            key={index}
                            navigation={navigation}
                            id={deal.id}
                            img='https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg'
                            name={deal.walker.given_name}
                            status={deal.status}
                            date={deal.workId.createdat}
                            deal={deal}
                          />
                        )
                      )}
                      
                      <View style={st.WaitWalkers_view}>
                        <IconAntD
                          color={Colors.txtBlack}
                          name='infocirlceo'
                          size={25}
                        />
                        <Text style={st.WaitWalkers_text}>Tu solicitud ha sido notificada a nuestros colaboradores, por favor espera su respuesta.</Text>
                      </View>
                    </View>
                  </ScrollView>
                          
                </Body>
              </CardItem>
            </Card>

            <Card>
              <CardItem header bordered>
                <View>
                  <Text style={st.TitleText}>Categoría</Text>
                  <Text>{work.work_type} - {work.service_type}</Text>
                </View>
              </CardItem>
              <CardItem bordered>
                <Body>
                  <View style={st.WorkAttributesList_view}>
                    <WorkAttribute title="Nombre de la mascota" desc={work.pet.name} />
                    <WorkAttribute title="Raza de la mascota" desc={work.pet.breed} />
                    <WorkAttribute title="Tamaño de la mascota" desc={work.pet.size} />
                    <WorkAttribute title="Fecha requerida de servicio" desc={dtf.inboxMsgFormat(work.createdat)} />
                    <WorkAttribute title="Ubicación" desc={work.location.address} />
                  </View>                  
                </Body>
              </CardItem>
              <CardItem footer bordered>
                <View style={st.WorkInfo_Footer}>
                  <Text>Publicado en</Text>
                  <Text>{dtf.inboxMsgFormat(work.createdat)}</Text>
                </View>
              </CardItem>
            </Card>
          </ScrollView>
        </Content>  
      </Container>
      
    )
  }

}

export default inject('appStore','forms')(observer(ViewSolicitudeScreen));
