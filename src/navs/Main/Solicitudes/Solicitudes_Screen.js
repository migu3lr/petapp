import React, { Component } from 'react';
import { StatusBar, Image, TouchableOpacity, KeyboardAvoidingView, ScrollView, TouchableHighlight } from 'react-native';
import { SwitchActions } from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';

import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3,
  List, ListItem
} from "native-base";
import OptionsMenu from 'react-native-options-menu';
import Icon from 'react-native-vector-icons/Ionicons'
import AsyncStorage from '@react-native-community/async-storage';
import { inject, observer } from 'mobx-react'

import { Theme, Colors } from 'petapp/src/styles/'
import st from './style'
import UserInformation from 'petapp/src/components/UserInformation'
import UserCarousel from 'petapp/src/components/UserCarousel'
import ContentPaper from 'petapp/src/components/ContentPaper'
import SectionTitle from 'petapp/src/components/SectionTitle'
import SolicitudeItemPreview from 'petapp/src/components/SolicitudeItemPreview'
import FilterButton from 'petapp/src/components/FilterButton'

import * as dtf from 'petapp/src/lib/datetime-format'

const FILTER_ALL = 'FILTER_ALL'
const FILTER_UNREAD = 'FILTER_UNREAD'
const FILTER_PROMOTION = 'FILTER_PROMOTION'
const FILTER_TODAY = 'FILTER_TODAY'

class DealScreen extends Component {

  sortDeals(){
    const { appStore } = this.props

    if(appStore.deals.length != 0) {
      return appStore.deals.slice().sort(function (a, b) {
        const a_last = a.lastChatMsg
        const b_last = b.lastChatMsg
        return b_last.sentAt - a_last.sentAt;
      });
    }
    return []
  }

  render() {
    const { appStore, vOpts } = this.props
    const OptionsBtn = (<Icon style={Theme.OptionsMenu_black} name="ios-more" />)
    let Work_List = appStore.works
    
    return (
      <Container>
        <StatusBar backgroundColor={Colors.bgColor} barStyle="light-content" />
        <Content contentContainerStyle={Theme.Content_view}>

          <ContentPaper
            title='Solicitudes activas'
            options={
              <OptionsMenu
                customButton={OptionsBtn}
                options={["Actualizar", "Ordenar", "Descartar todas"]}
                actions={[
                  () => appStore.getAllData(), 
                  () => alert(2), 
                  () => appStore.deleteInboxData()
                ]}
                buttonTextStyle={{ color: 'black' }}
              />
            }
          >

            {Work_List.length == 0 &&
              <View>
                <Text style={[Theme.sublabel_txt, { color: Colors.txtBlack }]}>No hay Solicitudes para revisar.</Text>
                <Text style={[Theme.sublabel_txt, { color: Colors.txtBlack }]} onPress={() => appStore.getAllData()}>
                  Actualizar
                </Text>
              </View>
            }

            <ScrollView>
              {Work_List.map(
                (work, index) => (
                  <SolicitudeItemPreview
                    key={index}
                    navigation={this.props.navigation}
                    id={work.id}
                    title={work.work_type}
                    description={'Paseo a 1 perro'}
                    ago='2min'
                    status={work.status}
                  />
                )
              )}
            </ScrollView>
              
            
          </ContentPaper>

        </Content>
      </Container >
    )
  }
}

export default inject('appStore', 'vOpts')(observer(DealScreen));

