import { StyleSheet } from 'react-native';
import {
  Colors,
  Fonts,
  Theme,
  //width, height
} from 'petapp/src/styles'


export default style = StyleSheet.create({
  FilterSection: {
    flexDirection: 'row',
    marginTop: 10
  },
  TitleText: {
    fontWeight:'bold', 
    fontSize: 16
  },
  WorkAttributesList_view: {
    flex: 1, 
    flexDirection: "column", 
  },
  WorkAttribute_view: {
    flex: 1, 
    flexDirection: "column", 
    marginBottom: 10
  },
  WorkAttributeDesc_view: {
    flex: 1, 
    flexDirection: "row"
  },
  WorkAttributeDesc_icon: {
    color:'#48d1cc', 
    marginRight: 10, 
    fontSize: 16
  },
  WalkersView: {
    flex: 1, 
    flexDirection: "row"
  },
  WalkerTouchable:{
    flexDirection: "column", 
    marginRight:20,
    alignItems: 'center'
  },
  WalkerDesc_text:{
    fontSize:12,
  },
  WorkInfo_Footer: {
    flex: 1, 
    flexDirection: "row", 
    justifyContent: "space-between"
  },
  WaitWalkers_view:{
    width: 110,
    height: null,
    backgroundColor: Colors.paperColor,
    alignItems: 'center',
    justifyContent: 'center',
    padding:5,
    borderRadius: 10,
  },
  WaitWalkers_text: {
    fontSize:10,
    textAlign: 'center'
  }
});