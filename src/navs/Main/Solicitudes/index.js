import React, {Component} from 'react';
import { createDrawerNavigator, createBottomTabNavigator, createStackNavigator, createSwitchNavigator } from 'react-navigation';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import OptionsMenu from 'react-native-options-menu';

import SolicitudesScreen from './Solicitudes_Screen'
import ViewSolicitudeScreen from './ViewSolicitude_Screen'
import UserProfileScreen from 'petapp/src/components/UserProfile'
import ChatScreen from 'petapp/src/components/Chat'


import * as st from 'petapp/src/styles'

const SolicitudesStack = createStackNavigator({
  Home: {
    screen: SolicitudesScreen,
    navigationOptions: ({navigation}) => {
      return {
        headerStyle: st.header.bar,
        headerLeft: (
          <Icon 
            style={{ paddingLeft: 10 }}
            onPress={() => navigation.openDrawer()}       
            name="user" 
            size={30} 
            color='#7C7C7C'
          />
        ),
        headerRight: (
          <Icon 
            style={{ paddingRight: 10 }}
            onPress={() => {navigation.openDrawer()}}       
            name="notification" 
            size={30} 
            color='#7C7C7C'
          />
        ),
        headerTitle: 'Servicios solicitados',
        headerTintColor: '#fff',
        headerTitleStyle: st.header.title,

      }
    },
  },
  ViewSolicitude: {
    screen: ViewSolicitudeScreen,
    navigationOptions: ({navigation}) => {
      return {
        headerStyle: st.header.bar,
        headerTitle: 'Solicitud de servicio',
        headerTintColor: '#fff',

      }
    },
  },
  UserProfile: {
    screen: UserProfileScreen,
    navigationOptions: ({navigation}) => {
      return {
        headerStyle: st.header.bar,
        headerTitle: 'Perfil',
        headerTintColor: '#fff',

      }
    },
  },
  Chat: {
    screen: ChatScreen,
    navigationOptions: ({navigation}) => {
      const deal = navigation.getParam('deal', {})

      return {
        headerStyle: st.header.barFullPaper,
        headerLeft: (
          <Icon 
            style={{ paddingLeft: 10 }}
            onPress={() => navigation.goBack()}       
            name="back" 
            size={20} 
            color='#7C7C7C'
          />
        ),
        headerRight: (
          
          <OptionsMenu
            customButton={
              <Icon 
                style={{ paddingRight: 10 }}     
                name="setting" 
                size={15} 
                color='#7C7C7C'
              />
            }
            options={["Aceptar esta negociación", "Ver perfil del cuidador", "Descartar esta negociación"]}
            actions={[
              () => deal.changeStatus('CONFIRMED'), 
              () => navigation.navigate('UserProfile', { walker: deal.walker }), 
              () => deal.changeStatus('REJECTED')
            ]}
          />
          
        ),
        headerTitle: `Chat: ${navigation.getParam('customerName', '#CustomerName')}`,
        headerTintColor: '#fff',
        headerTitleStyle: st.header.titleFullPaper,

      }
    },
  },
})

export default MainSwitch = createSwitchNavigator({
  Solicitudes: SolicitudesStack,
},{
  navigationOptions: ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
      tabBarVisible = false;
    }
  
    return {
      tabBarVisible
    };
  }
})