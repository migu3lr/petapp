import React, {Component} from 'react';
import { createDrawerNavigator, createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import { View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import IconFA from 'react-native-vector-icons/FontAwesome'

import HomeStack from './Home'
import SolicitudesStack from './Solicitudes'
import ActivityStack from './Activity'
import SideBar from 'petapp/src/components/Sidebar'



const MainTabNavigator = createBottomTabNavigator(
  {
    Inicio: HomeStack,
    Solicitudes: SolicitudesStack,
    Actividad: ActivityStack,
    //'Mi Cuenta': HomeStack,
  },
  {
    navigationOptions: ({ navigation }) => {
      const { routeName } = navigation.state.routes[navigation.state.index];
      return {
        header: null,
        headerTitle: routeName,
      }
    },
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ tintColor, }) => {
        const { routeName } = navigation.state;
        let iconName;
        IconRN = Icon
        switch (routeName) {
          case 'Inicio':
            iconName = 'home'
            break;
          case 'Solicitudes':
            iconName = 'calendar'
            break;
          case 'Actividad':
            iconName = 'paw'
            IconRN = IconFA
            break;
          case 'Mi Cuenta':
            iconName = 'message1'
            break;
        }      

        return (
          <IconRN
            color={tintColor} 
            name={iconName} 
            size={25}
          />
        )
      }
    }),
    tabBarOptions: {
      activeBackgroundColor: '#444D5D',
      inactiveBackgroundColor: '#444D5D',
      activeTintColor: '#E0E0E0',
      inactiveTintColor: 'gray',
      borderStyle: 'none',
      style: {height: 60, paddingBottom: 5, backgroundColor:'#444D5D'}
    }
  }
)

const MainStackNavigator = createStackNavigator(
  {
    MainTabNavigator
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerStyle: { backgroundColor: 'rgb(45, 52, 71)' },
        
      }
    }
  }
)

export default AppNavigator = createDrawerNavigator(
    {
      Main: MainStackNavigator
    },
    {
      contentComponent: props => <SideBar {...props} />
    }
  )