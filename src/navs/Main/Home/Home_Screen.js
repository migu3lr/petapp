import React, { Component } from 'react';
import { StatusBar, Image, TouchableOpacity, KeyboardAvoidingView, ScrollView, TouchableHighlight } from 'react-native';
import { SwitchActions } from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';

import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3,
  List, ListItem
} from "native-base";
import OptionsMenu from 'react-native-options-menu';
import Icon from 'react-native-vector-icons/Ionicons'
import AsyncStorage from '@react-native-community/async-storage';
import { inject, observer } from 'mobx-react'
import * as IoT from 'petapp/src/lib/aws-iot';
import st from 'petapp/src/styles/home'

import OverPet from 'petapp/src/components/Over_PetRegister'
import AS_Walk from 'petapp/src/components/AS_Walk'

import { Theme, home as st2, Colors } from 'petapp/src/styles/'
import UserInformation from 'petapp/src/components/UserInformation'
import UserCarousel from 'petapp/src/components/UserCarousel'
import InfoPaper from 'petapp/src/components/InfoPaper'
import ServiceList from 'petapp/src/components/ServiceList'

import SectionTitle from 'petapp/src/components/SectionTitle'
import InboxItemPreview from 'petapp/src/components/InboxItemPreview'


class HomeScreen extends Component {

  render() {

    return (
      <Container>
        <StatusBar backgroundColor={Colors.bgColor} barStyle="light-content" />
        <Content contentContainerStyle={Theme.Content_view}>
          <OverPet />
          <ScrollView style={Theme.Scroll_view}>
            <UserInformation name={this.props.authStore.user.full_name} />

            <UserCarousel />

            <SectionTitle title='Solicitar nuevo servicio:'/>
            <ServiceList customProps={this.props}/>

          </ScrollView>
        </Content>
      </Container>
        
    )
  }
}

export default inject('authStore','iotStore','forms')(observer(HomeScreen));

