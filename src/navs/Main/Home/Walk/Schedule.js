import React, {useState, Component} from 'react';
import { 
  Image, 
  View, 
  Text,
  TextInput, 
  ImageBackground, 
  StyleSheet, 
  Dimensions, 
  TouchableOpacity } from 'react-native';
import { 
  Container, 
  Button,
  Content,
  Header,
  Icon,
  Title, 
  Footer, 
  Left, 
  Right,
  Body } from 'native-base';
  import DateTimePicker from '@react-native-community/datetimepicker';
//import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
//import Icon from 'react-native-vector-icons/Ionicons'
import { SocialIcon } from 'react-native-elements'

import { inject, observer } from 'mobx-react'
const { width: WIDTH} = Dimensions.get('window')

class ScheduleScreen extends Component {  
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      mode: 'date',
      show: true
    };
  }
  
  setDate = (date) => this.setState({date}) 
  setMode = (mode) => this.setState({mode}) 
  setShow = (show) => this.setState({show}) 
  

  render() {

    const onChange = (event, selectedDate) => {
      const currentDate = selectedDate || this.state.date;
      this.setShow(false);
      this.setDate(currentDate.setSeconds(0,0));
    };
    
    const showMode = currentMode => {
      this.setShow(true);
      this.setMode(currentMode);
    };
    
    const showDatepicker = () => {
      showMode('date');
    };
    
    const showTimepicker = () => {
      showMode('time');
    };


    return (
      
      <Container>
        <Content padder>
          <View style={{ flex:1, alignItems:'center', justifyContent:'center'}}>
            <Text>Schedule</Text>
            
                <Button block rounded style={{alignSelf: "center", marginVertical:20, backgroundColor: "#FAB603" }}
                  onPress={() => showDatepicker()}
                >
                  <Text style={{marginHorizontal: 60, fontWeight:'bold', fontSize: 16}}>Define la fecha</Text>
                </Button>

                
             
                <Button block rounded style={{alignSelf: "center", marginVertical:20, backgroundColor: "#FAB603" }}
                  onPress={() => showTimepicker()}
                >
                  <Text style={{marginHorizontal: 60, fontWeight:'bold', fontSize: 16}}>¿A qué hora?</Text>
                </Button>

                <Text>Se programará para: {(new Date(this.state.date)).toString()}</Text>

                <Button block rounded style={{alignSelf: "center", marginVertical:20, backgroundColor: "#FAB603" }}
                  onPress={() => {
                    this.props.forms.walk.change_form('schedule', this.state.date)
                    this.props.navigation.navigate('WalkType')
                  }}
                >
                  <Text style={{marginHorizontal: 60, fontWeight:'bold', fontSize: 16}}>Continuar ></Text>
                </Button>
              
              {this.state.show && (
                <DateTimePicker
                  testID="dateTimePicker"
                  timeZoneOffsetInMinutes={0}
                  value={this.state.date}
                  mode={this.state.mode}
                  minimumDate={new Date()}
                  maximumDate={(new Date()).setDate((new Date()).getDate() + 7)}
                  is24Hour={false}
                  display="default"
                  onChange={onChange}
                />
              )}
           
          </View>
        </Content>  
        
      </Container>
      
    )
  }

}

export default inject('forms')(ScheduleScreen);
