import React, {Component} from 'react';
import { createDrawerNavigator, createBottomTabNavigator, createStackNavigator, createSwitchNavigator } from 'react-navigation';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import OptionsMenu from 'react-native-options-menu';

import ActivityScreen from './Activity_Screen'
import ViewSolicitudeScreen from './ViewSolicitude_Screen'
import ChatScreen from 'petapp/src/components/Chat'


import * as st from 'petapp/src/styles'

const ActivityStack = createStackNavigator({
  Home: {
    screen: ActivityScreen,
    navigationOptions: ({navigation}) => {
      return {
        headerStyle: st.header.bar,
        headerLeft: (
          <Icon 
            style={{ paddingLeft: 10 }}
            onPress={() => navigation.openDrawer()}       
            name="user" 
            size={30} 
            color='#7C7C7C'
          />
        ),
        headerRight: (
          <Icon 
            style={{ paddingRight: 10 }}
            onPress={() => {navigation.openDrawer()}}       
            name="notification" 
            size={30} 
            color='#7C7C7C'
          />
        ),
        headerTitle: 'Actividad',
        headerTintColor: '#fff',
        headerTitleStyle: st.header.title,

      }
    },
  },

})

export default MainSwitch = createSwitchNavigator({
  Activity: ActivityStack,
},{
  navigationOptions: ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
      tabBarVisible = false;
    }
  
    return {
      tabBarVisible
    };
  }
})