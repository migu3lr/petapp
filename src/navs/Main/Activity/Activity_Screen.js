import React, { Component } from 'react';
import { StatusBar, Image, TouchableOpacity, KeyboardAvoidingView, ScrollView, TouchableHighlight } from 'react-native';
import { SwitchActions } from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';

import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3,
  List, ListItem
} from "native-base";
import { Rating, AirbnbRating} from 'react-native-elements';
import OptionsMenu from 'react-native-options-menu';
import Icon from 'react-native-vector-icons/Ionicons'
import AsyncStorage from '@react-native-community/async-storage';
import { inject, observer } from 'mobx-react'
import MapView, {Marker,Polyline, PROVIDER_GOOGLE} from  'react-native-maps';

import { Theme, Colors } from 'petapp/src/styles/'
import st from './style'
import UserInformation from 'petapp/src/components/UserInformation'
import UserCarousel from 'petapp/src/components/UserCarousel'
import ContentPaper from 'petapp/src/components/ContentPaper'
import SectionTitle from 'petapp/src/components/SectionTitle'
import SolicitudeItemPreview from 'petapp/src/components/SolicitudeItemPreview'
import FilterButton from 'petapp/src/components/FilterButton'

import * as dtf from 'petapp/src/lib/datetime-format'

class ActivityScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      rating: 5
    }
    this.ratingCompleted = this.ratingCompleted.bind(this)
  }

  ratingCompleted(rating) {
    this.setState({rating})
  }

  static getDerivedStateFromProps(props, state) {
    const { appStore } = props
    if (!appStore.lastFinishedWork && appStore.ActiveWork && appStore.GeoLocationsbyWork(appStore.ActiveWork.id)) {
      return {
        region: {
          ...appStore.lastWorkGeolocation,
          latitudeDelta: Math.abs(0.5 * 2 / 111),
          longitudeDelta: Math.abs(0.5 / 111),
        },
        lineCoords: [...appStore.GeoLocationsbyWork(appStore.ActiveWork.id)]
      }
    }

    return {
      region: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: Math.abs(0.5 * 2 / 111),
        longitudeDelta: Math.abs(0.5 / 111),
      },
    }

  }

  render() {
    const { appStore, vOpts } = this.props
    const OptionsBtn = (<Icon style={Theme.OptionsMenu_black} name="ios-more" />)
    
    return (
      <Container>
        <StatusBar backgroundColor={Colors.bgColor} barStyle="light-content" />
        <Content contentContainerStyle={Theme.Content_view}>

          <ContentPaper
            title='Trabajo en progreso'
            options={
              <OptionsMenu
                customButton={OptionsBtn}
                options={["Actualizar", "Ordenar", "Descartar todas"]}
                actions={[
                  () => appStore.getAllData(), 
                  () => alert(2), 
                  () => appStore.deleteInboxData()
                ]}
                buttonTextStyle={{ color: 'black' }}
              />
            }
          >

            {!appStore.ActiveWork && !appStore.lastFinishedWork &&
              <View>
                <Text style={[Theme.sublabel_txt, { color: Colors.txtBlack }]}>No hay ningun trabajo activo.</Text>
                <Text style={[Theme.sublabel_txt, { color: Colors.txtBlack }]} onPress={() => appStore.getAllData()}>
                  Actualizar
                </Text>
              </View>
            }
            {appStore.ActiveWork && !appStore.lastFinishedWork &&
              <View style={st.map}>
                
                <MapView
                  provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                  style={st.map}
                  region={this.state.region}
                >
                  {appStore.GeoLocationsbyWork(appStore.ActiveWork.id) &&
                    <Polyline
                      coordinates={this.state.lineCoords}
                      strokeColor="#E5845C"
                      strokeWidth={4}
                    />
                  }
                </MapView>
                <ScrollView>
                  
                </ScrollView>
              </View>
            }

            {appStore.lastFinishedWork &&
              <View>
                <Text>Califica el servicio prestado:</Text>
                <AirbnbRating
                  count={5}
                  defaultRating={5}
                  onFinishRating={this.ratingCompleted}
                />

                <Button block rounded style={{alignSelf: "center", marginVertical:20, backgroundColor: "#FAB603" }}
                  onPress={() => {
                    appStore.lastFinishedWork.rate(this.state.rating)
                  }}
                >
                  <Text style={{marginHorizontal: 60, fontWeight:'bold', fontSize: 16}}>Enviar Calificación</Text>
                </Button>
                
              </View>
            }
            
          </ContentPaper>

        </Content>
      </Container >
    )
  }
}

export default inject('appStore', 'vOpts')(observer(ActivityScreen));

