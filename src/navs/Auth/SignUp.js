import React, {Component} from 'react';
import { 
  Image, 
  View, 
  Text,
  TextInput, 
  ImageBackground, 
  StyleSheet, 
  Dimensions, 
  TouchableOpacity,
  KeyboardAvoidingView,
  Picker } from 'react-native';
import { 
  Container, 
  Button,
  Content,
  Header,
  Icon,
  Title, 
  Footer, 
  Left, 
  Right,
  Body } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
//import Icon from 'react-native-vector-icons/Ionicons'
import { SocialIcon } from 'react-native-elements'

import { inject, observer } from 'mobx-react'

import bgImage from '../../images/signin_bg.jpg'
import logo from '../../images/logo.png'
import st from 'petapp/src/styles/signup'

const { width: WIDTH} = Dimensions.get('window')

class SignUpScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      showPass: true,
      press: false,
      doctype: ''
    }

    this.currentEmail = ''

  }

  async SignUp(creds, attributes) {
    const { authStore } = this.props
    //console.warn(attributes)
    this.currentEmail = creds.username
    await authStore.register(creds.username, creds.password, attributes)
    console.warn(authStore.username,authStore.notice,authStore.error)
    alert(authStore.notice || authStore.error)
    if(authStore.username) this.props.navigation.navigate('SignIn')
  }

  confirmSignUp(username, authCode) {
    /*await Auth.confirmSignUp(username, authCode)
    .then(() => {
      this.props.navigation.navigate('SignIn')
      console.log('Confirm sign up successful')
    })
    .catch(err => {console.log('Error codigo confirmacion: ', err)
      alert('Error codigo confirmacion: '+ JSON.stringify(err))
      
    })*/
  }

  resendSignUp(username) {
    /*await Auth.resendSignUp(username)
    .then(() => console.log('Confirmation code resent successfully'))
    .catch(err => {
      console.log('Error reenvio codigo: ', err)
      alert('Error codigo reenvio: '+ JSON.stringify(err))
    })*/
  }

  _showPass = () => {
    this.setState({
      showPass: !this.state.showPass,
      press: !this.state.press
    })
  }

  render() {
    const { navigation } = this.props;
    const profile = navigation.getParam('profile', 'customer');
    return (
      
      <Container>
        <ImageBackground source={bgImage} style={st.bgContainer}>
          <KeyboardAwareScrollView>
            <Content contentContainerStyle={st.Container} >
              <View style={st.header}>
                <Button transparent 
                onPress={() => this.props.navigation.goBack()}>
                  <Icon style={st.icon} name="arrow-back" />
                </Button>
                <Text style={st.title}>{`${profile == 'customer' ? 'Nueva cuenta' : 'Nuevo Colaborador'}`}</Text>
              </View>

              <View style={st.instrucciones}>
                <Text style={st.instruccionesTxt}>
                  {`Por favor ingresa los siguietes datos para idenficarte como uno de nuestros ${profile == 'customer' ? 'clientes' : 'colaboradores'}.`}
                </Text>
              </View>

              <View style={st.inputContainerRow}>
                <TextInput 
                  style={st.inputRow}
                  placeholder={'Nombre'}
                  placeholderTextColor={'rgba(0,0,0, 0.7)'}
                  underlineColorAndroid='transparent'
                  returnKeyLabel='next'
                  autoCapitalize='words'
                  onChangeText={e => this.given_name=e}
                />
                <TextInput 
                    style={st.inputRow}
                    placeholder={'Apellido'}
                    placeholderTextColor={'rgba(0,0,0, 0.7)'}
                    underlineColorAndroid='transparent'
                    returnKeyLabel='next'
                    autoCapitalize='words'
                    onChangeText={e => this.family_name=e}
                  />
              </View>

              {profile == 'walker' && 
                <View style={st.inputContainer}>
                  <View style={st.picker_wrapper}>
                    <Picker
                      selectedValue={this.state.doctype}
                      style={st.picker}
                      itemStyle={st.picker_item}
                      onValueChange={(e,i) => i != 0 && this.setState({doctype: e})}>
                      <Picker.Item label=" Seleccione un tipo de documento" value="" />
                      <Picker.Item label=" Cedula de ciudadanía" value="CC" />
                      <Picker.Item label=" Cedula de extranjería" value="CE" />
                      <Picker.Item label=" RUT/NIT" value="NIT" />
                    </Picker>
                  </View>
                </View>
              }
              {profile == 'walker' && 
                <View style={st.inputContainer}>
                  <Icon
                    name={'md-paper'}
                    size={28}
                    color={'#000'}
                    style={st.inputIcon}
                  />
                    <TextInput 
                      style={st.input}
                      placeholder={'Número de identificación'}
                      placeholderTextColor={'rgba(0,0,0, 0.7)'}
                      underlineColorAndroid='transparent'
                      returnKeyLabel='next'
                      autoCapitalize='none'
                      onChangeText={e => this.docnum=e}
                    />
                </View>
              }

              <View style={st.inputContainer}>
                <Icon
                  name={'md-mail'}
                  size={28}
                  color={'#000'}
                  style={st.inputIcon}
                />
                  <TextInput 
                    style={st.input}
                    placeholder={'Correo Electrónico'}
                    placeholderTextColor={'rgba(0,0,0, 0.7)'}
                    underlineColorAndroid='transparent'
                    returnKeyLabel='next'
                    autoCapitalize='none'
                    keyboardType={'email-address'}
                    onChangeText={e => this.email=e}
                  />
              </View>

              <View style={st.inputContainer}>
                <Icon
                  name={'md-lock'}
                  size={28}
                  color={'#000'}
                  style={st.inputIcon}
                />
                <TextInput 
                  style={st.input}
                  placeholder={'Contraseña'}
                  secureTextEntry={this.state.showPass}
                  placeholderTextColor={'rgba(0,0,0, 0.7)'}
                  underlineColorAndroid='transparent'
                  returnKeyLabel='done'
                  autoCapitalize='none'
                  onChangeText={e => this.password=e}
                />
                <TouchableOpacity style={st.btnEye}
                  onPress={this._showPass.bind(this)}>
                  <Icon 
                    name={this.state.press ? 'ios-eye' : 'ios-eye-off'}
                    size={26}
                    color={'rgba(0,0,0, 0.7)'}                  
                  />
                </TouchableOpacity>
              </View>

              <TouchableOpacity style={st.btnLogin}
                onPress={() => {
                  const creds = {
                    username: this.email,
                    password: this.password
                  }
                  const attributes = {
                    email: this.email,
                    'custom:doc_type': this.state.doctype || '',
                    'custom:doc_num': this.docnum || '',
                    given_name: this.given_name,
                    family_name: this.family_name,
                    profile: profile,
                    locale: 'es_CO',
                    zoneinfo: 'UTC-5',
                    updated_at: Date.now().toString(),
                    address: JSON.stringify({
                      country: 'Colombia'
                    })
                  }

                  this.SignUp(creds,{...attributes})
                }}>
                <Text style={st.textLogin}>Registrarme</Text>
              </TouchableOpacity>              

            </Content>
          </KeyboardAwareScrollView>      
        </ImageBackground>
      </Container>
      
    )
  }

}

export default inject('authStore')(SignUpScreen);

const styles = StyleSheet.create({
  title: {
    //height: 50,
  },
  Container: {
    flex:1,
    alignItems: 'center',
    justifyContent:'center',

  },
  bgContainer: {
    flex: 1,
    width: null,
    height: null,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logoContainer: {
    alignItems: 'center'
  },
  logo: {
    width: 120,
    height: 120
  },
  logoText: {
    color: 'white',
    fontSize: 40,
    fontWeight: '500',
    marginTop: 10,
    opacity: 0.8
  },
  inputContainerRow: {
    //flex:1,
    width: WIDTH -55,
    marginTop: 10,
    flexDirection:'row',
    justifyContent:'space-between',   
  },
  inputRow: {
    width: (WIDTH - 65)/2,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 10,
    backgroundColor: 'rgba(255,255,255, 0.5)',
    color: 'rgba(0,0,0, 0.8)',
  },
  inputCode: {
    width: WIDTH - 115,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(255,255,255, 0.5)',
    color: 'rgba(0,0,0, 0.8)',
  },
  inputContainerCode: {
    width: WIDTH - 100
  },
  inputIconCode: {
    position: 'absolute',
    top: 8,
    left: 15
  },
  inputContainer: {
    marginTop: 10
  },
  input: {
    width: WIDTH - 55,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(255,255,255, 0.5)',
    color: 'rgba(0,0,0, 0.8)',
    marginHorizontal: 25
  },
  inputIcon: {
    position: 'absolute',
    top: 8,
    left: 37
  },
  btnEye: {
    position: 'absolute',
    top: 8,
    right: 37
  },
  btnResend: {
    width: 45,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#FBB45F',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnLogin: {
    width: WIDTH - 55,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#FBB45F',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20
  },  
  textLogin: {
    color: '#6F5F4E',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: '500',
  },
  optionLinksContainer: {
    width: WIDTH - 70,
    height: 60,
    justifyContent: 'center'
    
  },
  optionLink: {
    color: 'rgba(255,255,255,1)', 
    fontSize: 16
  },
  olLeft: {
    position: 'absolute',
    left: 0
  },
  olRight: {
    position: 'absolute',
    right: 0
  },
  authLinksContainer: {
    flexDirection: 'row',    
  },
  textAuthOp: {
    color: 'white', 
    textAlignVertical:'center'
  },
  desc: {
    color: 'white',
    fontSize: 14,
    textAlign:'justify'
  },
  descContainer: {
   // width: WIDTH - 70
  }
})