import React, {Component} from 'react';
import { 
  Image, 
  View, 
  TextInput, 
  ImageBackground, 
  StyleSheet, 
  Dimensions, 
  TouchableOpacity,
  KeyboardAvoidingView } from 'react-native';
import { 
  Container, Header, Title, Left, Icon, Right, Body, Content,
  Button,
  Card, CardItem, 
  Thumbnail,
  Text, H1, H2, H3  } from 'native-base';

import bgImage from '../../images/signin_bg.jpg'
import st from 'petapp/src/styles/profile'

const { width: WIDTH} = Dimensions.get('window')

class ProfileScreen extends Component {

  render() {
    const walker = 'petapp/src/images/walker.jpg'
    const customer = 'petapp/src/images/customer.jpg'

    return (
      
      <Container>
        <ImageBackground source={bgImage} style={st.bgContainer}>
            <Content contentContainerStyle={st.Container} >
              <View style={st.header}>
                <Button transparent 
                onPress={() => this.props.navigation.goBack()}>
                  <Icon style={st.icon} name="arrow-back" />
                </Button>
                <Text style={st.title}>Registrar nuevo usuario</Text>
              </View>
              <View style={st.vwServicios}>
                <TouchableOpacity style={st.btServicio}
                onPress={() => this.props.navigation.navigate('SignUp', {profile: 'customer'})}>
                  <Thumbnail style={st.thumbServicio} source={require(customer)} />
                  <Text style={st.txtServicio}>¡Soy un cliente!</Text>
                </TouchableOpacity>
                <TouchableOpacity style={st.btServicio}
                onPress={() => this.props.navigation.navigate('SignUp', {profile: 'walker'})}>
                  <Thumbnail style={st.thumbServicio} source={require(walker)} />
                  <Text style={st.txtServicio}>Colaboradores</Text>
                </TouchableOpacity>  
              </View>

            </Content>     
        </ImageBackground>
      </Container>
      
    )
  }

}

export default ProfileScreen;
