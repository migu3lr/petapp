import { createStackNavigator } from 'react-navigation';

import SignInScreen from './SignIn';
import SignUpScreen from './SignUp';
import ProfileScreen from './Profile';
import ForgotPwdScreen from './ForgotPwd'

export default AuthStack = createStackNavigator(
  { 
    SignIn: SignInScreen,
    SignUp: SignUpScreen,
    Profile: ProfileScreen,
    ForgotPwd: ForgotPwdScreen
  },
  {
    headerMode: 'none'
  }
  );