import React, {Component} from 'react';
import { ActivityIndicator, StatusBar, View } from 'react-native';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content,Text, Card, CardItem } from "native-base";
import AsyncStorage from '@react-native-community/async-storage';
//import Auth from '@aws-amplify/auth'
import { inject, observer } from 'mobx-react'
import * as Mobx from 'mobx'

import * as IoT from '../lib/aws-iot';

  class AuthLoadingScreen extends Component {
    async componentDidMount() {
      const { iotStore, appStore, authStore } = this.props

      await this.validateUserSession();

      const connectCallback = () => iotStore.deviceConnectedStatusChanged(true);
      const closeCallback = () => iotStore.deviceConnectedStatusChanged(false);
      await iotStore.acquirePublicPolicies(connectCallback, closeCallback);

      //console.warn(iotStore, appStore, Mobx.toJS(authStore))

      Mobx.when(() => authStore.loggedIn && appStore.enterApp , 
        () => this.props.navigation.navigate(authStore.user.profile == 'walker' ? 'CoApp' : 'App'))

      Mobx.when(() => !authStore.loggedIn || !appStore.enterApp,
        () => this.props.navigation.navigate('Auth'))
      
    }

    async validateUserSession() {
      const { authStore } = this.props
      var isLoggedIn = await AsyncStorage.getItem('isLoggedIn')
      if (isLoggedIn === 'true') {
        authStore.loggedInStatusChanged(true);
      } else {
        authStore.loggedInStatusChanged(false);
      }
    }

    render() {
      return(
        <View>
          <ActivityIndicator />
          <StatusBar backgroundColor="#222B3A" barStyle='light-content' />   
        </View>
      )
    }
  }

export default inject('appStore','iotStore','authStore')(observer(AuthLoadingScreen));
//export default AuthLoadingScreen