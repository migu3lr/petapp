import React, {Component} from 'react';
import { Image, TouchableOpacity, KeyboardAvoidingView, ScrollView,TouchableHighlight } from 'react-native';
import { SwitchActions } from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';

import { 
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem, 
  Thumbnail,
  Text, H1, H2, H3,
  List, ListItem
} from "native-base";
import OptionsMenu from 'react-native-options-menu';
import Icon from 'react-native-vector-icons/Ionicons'
import AsyncStorage from '@react-native-community/async-storage';
import { inject, observer } from 'mobx-react'

import {home as st} from 'petapp/src/styles/'


class HomeScreen extends Component {

  closeRow(rowMap, rowKey) {
    if (rowMap[rowKey]) {
        rowMap[rowKey].closeRow();
        
    }
  }

  deleteRow(rowMap, rowKey) {
    const { appStore } = this.props
    this.closeRow(rowMap, rowKey);
    
    appStore.deleteInboxMsg(rowKey)    
  }

  onSwipeValueChange = (swipeData) => {
    const { key, value } = swipeData;
    const { appStore } = this.props
    // 375 or however large your screen is (i.e. Dimensions.get('window').width)
    if (value < -375 && !this.animationIsRunning) {
      this.animationIsRunning = true;
      appStore.deleteInboxMsg(key)    
      this.animationIsRunning = false;
    }
  }

  render() {
    const { appStore } = this.props
    const OptionsBtn = (<Icon style={st.sectionOpts} name="ios-more" />)
    return (
      <Container>
        <Content padder contentContainerStyle={st.container}>
          <ScrollView>
            <View style={st.sectionHeader}>
              <Text style={st.sectionTitle}>Trabajos </Text>
              <OptionsMenu
                customButton={OptionsBtn}
                options={["Actualizar", "Ordenar", "Descartar todas"]}
                actions={[
                  () => appStore.getAllData(), 
                  () => alert(2), 
                  () => appStore.deleteInboxData()
                ]}
              />
            </View>
            {appStore.deals.length > 0 && 
              <SwipeListView
                data={appStore.deals}
                renderItem={ (d, rowMap) => (
                  <SwipeRow
                    rightOpenValue={-375}
                    onSwipeValueChange={this.onSwipeValueChange}
                    swipeKey={d.item.id}
                    disableRightSwipe={true}
                  >
                    <View style={st.rowBack}>
                      <Text></Text>
                      <View style={st.backRightBtn}>
                        <Icon style={st.backRightIcon} name="md-trash" />
                        <Text>Borrar</Text>
                      </View>
                      
                    </View>
                    <TouchableHighlight underlayColor={'#AAA'} onPress={() => this.props.navigation.navigate('ViewWork', {d})} style={st.rowFront}>
                      <Card>
                        <CardItem style={st.workTitle}>
                          <Thumbnail source={require('petapp/src/images/customer.jpg')} />
                          <Left>
                            <Body>
                              <Text style={st.txt}>Work Id: {d.item.workId.id}</Text>
                              <Text note style={st.workDescTitle}>Tipo de Trabajo:{d.item.workId.workType}</Text>
                            </Body>
                          </Left>
                          <Right>
                            <Text>Dist: 10 Km</Text>
                            <Text>Hace 10h</Text>
                          </Right>
                        </CardItem>
                      </Card>
                    </TouchableHighlight>
                  </SwipeRow>
                )}
              /> 
            }

            {appStore.deals.length == 0 &&
              <Text style={st.regular}>No hay trabajos aceptados</Text>
            }

          </ScrollView>
        </Content>
      </Container>
    )
  }
}

export default inject('authStore','iotStore','appStore','forms')(observer(HomeScreen));

