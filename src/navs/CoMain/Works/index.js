import React, {Component} from 'react';
import { createDrawerNavigator, createBottomTabNavigator, createStackNavigator, createSwitchNavigator } from 'react-navigation';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'

import WorksScreen from './Works_Screen'
import ViewWorkScreen from './ViewWork_Screen';


import * as st from 'petapp/src/styles'

const WorksStack = createStackNavigator({
  Works: {
    screen: WorksScreen,
    navigationOptions: ({navigation}) => {
      return {
        headerStyle: st.header.bar,
        headerLeft: (
          <Icon 
            style={{ paddingLeft: 10 }}
            onPress={() => navigation.openDrawer()}       
            name="md-menu" 
            size={30} 
            color='white'
          />
        ),

      }
    },
  },
  ViewWork: {
    screen: ViewWorkScreen,
    navigationOptions: ({navigation}) => {
      return {
        headerStyle: st.header.bar,
        headerTitle: 'Solicitud de servicio',
        headerTintColor: '#fff',

      }
    },
  }
})

export default MainSwitch = createSwitchNavigator({
  Works: WorksStack,
},{
  navigationOptions: ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
      tabBarVisible = false;
    }
  
    return {
      tabBarVisible
    };
  }
})