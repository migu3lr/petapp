import React, {Component} from 'react';
import { 
  Image, 
  View, 
  Text,
  TextInput, 
  ImageBackground, 
  StyleSheet, 
  Dimensions, 
  TouchableOpacity,
  ScrollView } from 'react-native';
import { 
  Container, 
  Button,
  Content,
  Card,
  CardItem,
  Icon,
  Title, 
  Footer, 
  List, ListItem,
  Left, 
  Right,
  Body } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
//import Icon from 'react-native-vector-icons/Ionicons'
import { SocialIcon } from 'react-native-elements'

import * as dtf from 'petapp/src/lib/datetime-format'

import { inject, observer } from 'mobx-react'

import {home as st} from 'petapp/src/styles/'

const { width: WIDTH} = Dimensions.get('window')

class WorkAttribute extends Component {
  render() {
    const { title, desc } = this.props
    if (desc) { 
      return (
        <View style={{flex: 1, flexDirection: "column", marginBottom: 10}}>
          <Text style={{fontWeight:'bold', fontSize: 16}}>{title} </Text>
          <View style={{flex: 1, flexDirection: "row"}}>
              <Icon style={{color:'#48d1cc', marginRight: 10, fontSize: 16}} name="arrow-forward" />
              <Text>{desc}</Text> 
          </View>
        </View>
      )
    }
    return null
  }
}

class ViewWorkScreen extends Component {
  render() {
    const { appStore, forms, navigation } = this.props

    const id = navigation.getParam('id', '');

    const work = appStore.WorkbyID(id);


    if(work == undefined) {
      return (
      
        <Container>
          <Content padder contentContainerStyle={st.container}>
            <ScrollView>
              <Card>
                <CardItem header bordered>
                  <View style={{flex: 1, flexDirection: "column"}}>
                    <Text style={{fontWeight:'bold', fontSize: 16}}>ERROR al buscar mensaje.</Text>
                  </View>
                </CardItem>                
              </Card>
            </ScrollView>
          </Content>  
        </Container>
        
      )
    }

    
    return (
      
      <Container>
        <Content padder contentContainerStyle={st.container}>
          <ScrollView>
            <Card>
              <CardItem header bordered>
                <View style={{flex: 1, flexDirection: "column"}}>
                  <Text style={{fontWeight:'bold', fontSize: 16}}>Categoría</Text>
                  <Text>{work.work_type} - {work.service_type}</Text>
                </View>
              </CardItem>
              <CardItem bordered>
                <Body>
                  <View style={{flex: 1, flexDirection: "column"}}>
                    <WorkAttribute title="Nombre del cliente" desc={work.customer.full_name} />
                    <WorkAttribute title="Nombre de la mascota" desc={work.pet.name} />
                    <WorkAttribute title="Raza de la mascota" desc={work.pet.breed} />
                    <WorkAttribute title="Tamaño de la mascota" desc={work.pet.size} />
                    <WorkAttribute title="Fecha requerida de servicio" desc={dtf.inboxMsgFormat(work.createdat)} />
                    <WorkAttribute title="Ubicación" desc={work.location.address} />
                    <WorkAttribute title="Cuidador asignado" desc={work.walker ? work.walker.identityId : 'Sin confirmar'} />
                  </View>                  
                </Body>
              </CardItem>
              <CardItem footer bordered>
                <View style={{flex: 1, flexDirection: "row", justifyContent: "space-between"}}>
                  <Text>Publicado en</Text>
                  <Text>{dtf.inboxMsgFormat(work.createdat)}</Text>
                </View>
              </CardItem>
            </Card>

            {work.status == 'PENDING' &&
              <Button block rounded style={{alignSelf: "center", marginVertical:20, backgroundColor: "#48d1cc" }}
                onPress={() => {
                  work.deal()
                }}
              >
                <Text style={{marginHorizontal: 60, fontWeight:'bold', fontSize: 16}}>Aceptar trabajo</Text>
              </Button>
            }
            {work.status == 'CONFIRMED' &&
              <Button block rounded style={{alignSelf: "center", marginVertical:20, backgroundColor: "#FAB603" }}
                onPress={() => {
                  work.start()
                }}
              >
                <Text style={{marginHorizontal: 60, fontWeight:'bold', fontSize: 16}}>Iniciar trabajo</Text>
              </Button>
            }
            {work.status == 'IN_PROGRESS' &&
              <Button block rounded style={{alignSelf: "center", marginVertical:20, backgroundColor: "#FAB603" }}
                onPress={() => {
                  work.finish()
                }}
              >
                <Text style={{marginHorizontal: 60, fontWeight:'bold', fontSize: 16}}>Finalizar trabajo</Text>
              </Button>
            }
      
          </ScrollView>
        </Content>  
      </Container>
      
    )
  }

}

export default inject('appStore','forms')(observer(ViewWorkScreen));
