import React, { Component } from 'react';
import { createDrawerNavigator, createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import { StatusBar, View, Text, StyleSheet } from 'react-native';
//import Icon from 'react-native-vector-icons/Ionicons'
import Icon from 'react-native-vector-icons/AntDesign'

import HomeStack from './Home'
import WorkStack from './Works'
import InboxStack from './Inbox'
import DealStack from './Deal'
import SideBar from 'petapp/src/components/Sidebar'

import * as st from 'petapp/src/styles'
import { Colors } from 'react-native/Libraries/NewAppScreen';


const MainTabNavigator = createBottomTabNavigator(
  {
    Inicio: HomeStack,
    //Trabajos: WorkStack,
    Mensajes: InboxStack,
    Negociacion: DealStack,
  },
  {
    navigationOptions: ({ navigation }) => {
      const { routeName } = navigation.state.routes[navigation.state.index];
      return {
        header: null,
        headerTitle: routeName,
      }
    },
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ tintColor, }) => {
        const { routeName } = navigation.state;
        let iconName;
        switch (routeName) {
          case 'Inicio':
            iconName = 'home'
            break;
          case 'Trabajos':
            iconName = 'calendar'
            break;
          case 'Mensajes':
            iconName = 'mail'
            break;
          case 'Negociacion':
            iconName = 'message1'
            break;
        }      

        return (
          <Icon
            color={tintColor}
            name={iconName}
            size={25}
          />
        )
      }
    }),
    tabBarOptions: {
      activeBackgroundColor: '#444D5D',
      inactiveBackgroundColor: '#444D5D',
      activeTintColor: '#E0E0E0',
      inactiveTintColor: 'gray',
      borderStyle: 'none',
      style: {height: 60, paddingBottom: 5, backgroundColor:'#444D5D'}
    }
  }
)

const MainStackNavigator = createStackNavigator(
  {
    MainTabNavigator
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerStyle: { backgroundColor: 'rgb(45, 52, 71)' },
        
      }
    }
  }
)

export default AppNavigator = createDrawerNavigator(
  {
    Main: MainStackNavigator
  },
  {
    contentComponent: props => <SideBar {...props} />
  }
)