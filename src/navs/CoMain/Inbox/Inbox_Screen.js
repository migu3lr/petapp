import React, { Component } from 'react';
import { StatusBar, Image, TouchableOpacity, KeyboardAvoidingView, ScrollView, TouchableHighlight } from 'react-native';
import { SwitchActions } from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';

import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3,
  List, ListItem
} from "native-base";
import OptionsMenu from 'react-native-options-menu';
import Icon from 'react-native-vector-icons/Ionicons'
import AsyncStorage from '@react-native-community/async-storage';
import { inject, observer } from 'mobx-react'

import { Theme, Colors } from 'petapp/src/styles/'
import st from './style'
import UserInformation from 'petapp/src/components/UserInformation'
import UserCarousel from 'petapp/src/components/UserCarousel'
import ContentPaper from 'petapp/src/components/ContentPaper'
import SectionTitle from 'petapp/src/components/SectionTitle'
import InboxItemPreview from 'petapp/src/components/InboxItemPreview'
import FilterButton from 'petapp/src/components/FilterButton'

const FILTER_ALL = 'FILTER_ALL'
const FILTER_UNREAD = 'FILTER_UNREAD'
const FILTER_PROMOTION = 'FILTER_PROMOTION'
const FILTER_TODAY = 'FILTER_TODAY'

class InboxScreen extends Component {

  render() {
    const { appStore, vOpts } = this.props
    const OptionsBtn = (<Icon style={Theme.OptionsMenu_black} name="ios-more" />)
    let Inbox_List = appStore.inbox
    
    switch (vOpts.Inbox.filter) {
      case FILTER_ALL: 
        Inbox_List = appStore.inbox
        break;
      case FILTER_UNREAD: 
        Inbox_List = appStore.inbox.filter(m => m && m.status == 'UNREAD')
        break;
      case FILTER_PROMOTION: 
        Inbox_List = appStore.inbox.filter(m => m && m.type == 'promotion')
        break;
      case FILTER_TODAY: 
        Inbox_List = appStore.inbox
        break;
    
      default:
        vOpts.Inbox.updateFilter(FILTER_ALL)
        break;
    }
    
    return (
      <Container>
        <StatusBar backgroundColor={Colors.bgColor} barStyle="light-content" />
        <Content contentContainerStyle={Theme.Content_view}>

          <ContentPaper
            title='Bandeja de Entrada'
            subtitle={`${appStore.inbox.length} Mensajes, ${appStore.inbox.length -3} Sin leer`}
            options={
              <OptionsMenu
                customButton={OptionsBtn}
                options={["Actualizar", "Ordenar", "Descartar todas"]}
                actions={[
                  () => appStore.getAllData(), 
                  () => alert(2), 
                  () => appStore.deleteInboxData()
                ]}
                buttonTextStyle={{ color: 'black' }}
              />
            }
          >
            <View style={st.FilterSection}>
              <FilterButton 
                text='Ver todos' 
                FilterAction={() => vOpts.Inbox.updateFilter(FILTER_ALL)}
                active={vOpts.Inbox.filter == FILTER_ALL}
              />
              <FilterButton 
                text='Sin leer'
                FilterAction={() => vOpts.Inbox.updateFilter(FILTER_UNREAD)}
                active={vOpts.Inbox.filter == FILTER_UNREAD}
              />
              <FilterButton 
                text='De hoy'
                FilterAction={() => vOpts.Inbox.updateFilter(FILTER_TODAY)}
                active={vOpts.Inbox.filter == FILTER_TODAY}
              />
              <FilterButton 
                text='Promociones'
                FilterAction={() => vOpts.Inbox.updateFilter(FILTER_PROMOTION)}
                active={vOpts.Inbox.filter == FILTER_PROMOTION}
              />
            </View>

            {Inbox_List.length == 0 &&
              <View>
                <Text style={[Theme.sublabel_txt, { color: Colors.txtBlack }]}>No hay Mensajes.</Text>
                <Text style={[Theme.sublabel_txt, { color: Colors.txtBlack }]} onPress={() => appStore.getAllData()}>
                  Actualizar
                </Text>
              </View>
            }

            <ScrollView>
              {Inbox_List.map(
                (msg, index) => (
                  <InboxItemPreview
                    key={index}
                    navigation={this.props.navigation}
                    id={msg.workId.id}
                    sender={msg.customer_name}
                    subject={msg.work_type}
                    ago='2min'
                    distance='1km'
                    priceRange='10K-20K'
                  />
                )
              )}
            </ScrollView>
              
            
          </ContentPaper>

        </Content>
      </Container >
    )
  }
}

export default inject('appStore', 'vOpts')(observer(InboxScreen));

