import React, { Component } from 'react';
import { StatusBar, Image, TouchableOpacity, KeyboardAvoidingView, ScrollView, TouchableHighlight } from 'react-native';
import { SwitchActions } from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';

import {
  Container, Header, Title, Left, Right, Body, Content, View,
  Button,
  Card, CardItem,
  Thumbnail,
  Text, H1, H2, H3,
  List, ListItem
} from "native-base";
import OptionsMenu from 'react-native-options-menu';
import Icon from 'react-native-vector-icons/Ionicons'
import AsyncStorage from '@react-native-community/async-storage';
import { inject, observer } from 'mobx-react'

import { Theme, home as st, Colors } from 'petapp/src/styles/'
import UserInformation from 'petapp/src/components/UserInformation'
import UserCarousel from 'petapp/src/components/UserCarousel'
import InfoPaper from 'petapp/src/components/InfoPaper'
import SectionTitle from 'petapp/src/components/SectionTitle'
import WorkItemPreview from 'petapp/src/components/WorkItemPreview'


class HomeScreen extends Component {

  render() {
    const { appStore } = this.props
    return (
      <Container>
        <StatusBar backgroundColor={Colors.bgColor} barStyle="light-content" />
        <Content contentContainerStyle={Theme.Content_view}>
          <ScrollView style={Theme.Scroll_view}>
            <UserInformation name={this.props.authStore.user.full_name} />

            <UserCarousel />

            <SectionTitle title='Hoy' />
            <InfoPaper>
              {appStore.works.map(
                (work, index) => {
                  if (work.status == 'CONFIRMED' || work.status == 'IN_PROGRESS')
                    return (
                      <WorkItemPreview
                        key={index}
                        navigation={this.props.navigation}
                        id={work.id}
                        sender={work.customer.full_name}
                        subject={work.work_type}
                        ago='2min'
                        distance='1km'
                        priceRange='10K-20K'
                        status={work.status}
                      />
                    )
                }
              )}
              {appStore.inbox.length == 0 &&
                <View>
                  <Text style={[Theme.sublabel_txt, {color: Colors.txtBlack}]}>No hay notificaciones.</Text>
                  <Text style={[Theme.sublabel_txt, {color: Colors.txtBlack}]} onPress={() => appStore.getAllData()}>
                    Actualizar
                  </Text>
                </View>
              }
            </InfoPaper>

          </ScrollView>
        </Content>
      </Container >
    )
  }
}

export default inject('authStore', 'iotStore', 'appStore', 'forms')(observer(HomeScreen));

