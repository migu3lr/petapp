import React, {Component} from 'react';
import { createDrawerNavigator, createBottomTabNavigator, createStackNavigator, createSwitchNavigator } from 'react-navigation';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import OptionsMenu from 'react-native-options-menu';

import DealScreen from './Deal_Screen'
import ChatScreen from 'petapp/src/components/Chat'


import * as st from 'petapp/src/styles'

const DealStack = createStackNavigator({
  Home: {
    screen: DealScreen,
    navigationOptions: ({navigation}) => {
      return {
        headerStyle: st.header.bar,
        headerLeft: (
          <Icon 
            style={{ paddingLeft: 10 }}
            onPress={() => navigation.openDrawer()}       
            name="user" 
            size={30} 
            color='#7C7C7C'
          />
        ),
        headerRight: (
          <Icon 
            style={{ paddingRight: 10 }}
            onPress={() => {navigation.openDrawer()}}       
            name="notification" 
            size={30} 
            color='#7C7C7C'
          />
        ),
        headerTitle: 'Salas de Negociación',
        headerTintColor: '#fff',
        headerTitleStyle: st.header.title,

      }
    },
  },
  Chat: {
    screen: ChatScreen,
    navigationOptions: ({navigation}) => {
      const prueba = navigation.getParam('prueba', ()=>{})
      return {
        headerStyle: st.header.barFullPaper,
        headerLeft: (
          <Icon 
            style={{ paddingLeft: 10 }}
            onPress={() => navigation.goBack()}       
            name="back" 
            size={20} 
            color='#7C7C7C'
          />
        ),
        headerRight: (
          
          <OptionsMenu
            customButton={
              <Icon 
                style={{ paddingRight: 10 }}     
                name="setting" 
                size={15} 
                color='#7C7C7C'
              />
            }
            options={["Cambiar precio", "Ver perfil del Cliente", "Cancelar negociación"]}
            actions={[
              () => prueba(), 
              () => alert(2), 
              () => alert(3)
            ]}
          />
          
        ),
        headerTitle: `Chat: ${navigation.getParam('customerName', '#CustomerName')}`,
        headerTintColor: '#fff',
        headerTitleStyle: st.header.titleFullPaper,

      }
    },
  },
})

export default MainSwitch = createSwitchNavigator({
  Deal: DealStack,
},{
  navigationOptions: ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
      tabBarVisible = false;
    }
  
    return {
      tabBarVisible
    };
  }
})