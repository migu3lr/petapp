import { StyleSheet, Dimensions } from 'react-native';

const { width: WIDTH} = Dimensions.get('window')

const styles = StyleSheet.create({
  header_bar: {
    backgroundColor: 'rgb(45, 52, 71)',
  },
  container: {
    flex: 1,
    backgroundColor: 'rgb(45, 52, 71)',
  },
  titulo: {
    paddingBottom:10,
    fontSize:46,
    fontFamily: 'Calibre-Semibold',
    color: 'white'
  },
  regular: {
    fontSize:25,
    fontFamily: 'SF-Pro-Text-Regular',
    color: 'white'
  },
  vwServicios: {
    flex:1,
    alignContent: 'space-around',
    flexWrap: 'wrap',
    flexDirection:'row',
    justifyContent:'space-around', 
  
  },
  btServicio: {
    marginHorizontal: 20,
    marginVertical: 10

  },
  thumbServicio: {
    height: 120, 
    width: 120, 
    borderRadius: 120,
  },
  txtServicio: {
    textAlign: 'center'
  },
});

export default styles;
