import { StyleSheet, Dimensions } from 'react-native';

const { width: WIDTH} = Dimensions.get('window')

const styles = StyleSheet.create({
  header_bar: {
    backgroundColor: 'rgb(45, 52, 71)',
  },
});

export default styles;
