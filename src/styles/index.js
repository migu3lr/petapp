import { StyleSheet, Dimensions } from 'react-native';

export const IS_IOS = Platform.OS === 'ios';
export const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export const Colors = {
  bgColor: '#222B3A' //'rgb(45, 53, 66)'
  , cardColor: '#737C8E'
  , cardButton: '#2C343A'
  , cardIcon: '#7C7C7C'
  , paperColor: '#E0E0E0'
  , bottomBar: '#444D5D'
  , barButton: '#E0E0E0'
  , iconColor: '#FAB603'
  , txtColor: 'white'
  , txtBlack: '#3C3C3C'
  , txtBlue: '#48d1cc'
}

export const Fonts = {
  title: 'Calibre-Semibold'
  , content: 'SF-Pro-Text-Regular'
  , titleSize: 46
  , contentSize: 25
  , sublabelSize: 16
  , regularSize: 12
  , litleSize: 10
}

export const Theme = StyleSheet.create({
  Content_view: {
    flex: 1,
    backgroundColor: Colors.bgColor
  },
  Scroll_view: {
    overflow: 'visible'
  },
  label_txt: {
    fontFamily: Fonts.title,
    color: Colors.txtColor
  },
  sublabel_txt: {
    fontFamily: Fonts.content,
    color: Colors.txtColor,
    fontSize: Fonts.sublabelSize
  },
  content_txt: {
    fontSize: Fonts.contentSize,
    fontFamily: Fonts.content,
    color: Colors.txtColor
  },
  titleBar: {
    fontFamily: Fonts.title,
    fontSize: Fonts.contentSize,
  },
  OptionsMenu: {
    color: Colors.txtColor,
    fontSize: Fonts.titleSize,
    alignSelf: 'center',
  },
  OptionsMenu_black: {
    fontSize: Fonts.titleSize,
    alignSelf: 'center',
    color: Colors.txtBlack,
  }
})

export const infoPaper_st = StyleSheet.create({
  title: {
    ...Theme.label_txt,
    fontSize: Fonts.contentSize,
    //paddingVertical: 20,
    
  }
});

export const header = StyleSheet.create({
  bar: {
    backgroundColor: Colors.bgColor,
    height: 60,
    elevation: 0,
    shadowOpacity: 0,
  },
  barFullPaper: {
    backgroundColor: Colors.bgColor,
    height: 30,
    elevation: 0,
    shadowOpacity: 0,
    
  },
  title: {
    ...Theme.titleBar,
    textAlign: 'center',
    flex: 1
  },
  titleFullPaper: {
    ...Theme.titleBar,
    textAlign: 'center',
    flex: 1,
    fontSize: Fonts.sublabelSize,
  }
});

export const footer = StyleSheet.create({
  tabs: {
    backgroundColor: Colors.bottomBar,
  },
});

export const home = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bgColor,
    //overflow: 'visible'
    //paddingHorizontal: 20
  },
  sectionHeader: {
    flexDirection: 'row',
  },
  sectionOpts: {
    color: Colors.txtColor,
    fontSize: Fonts.titleSize,
    alignSelf: 'center',
  },
  sectionTitle: {
    paddingBottom: 10,
    fontSize: Fonts.titleSize,
    fontFamily: Fonts.title,
    color: Colors.txtColor
  },
  regular: {
    fontSize: Fonts.contentSize,
    fontFamily: Fonts.content,
    color: Colors.txtColor
  },
  txt: {
    fontFamily: Fonts.content,
  },
  vwServicios: {
    flex: 1,
    alignContent: 'space-around',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-around',

  },
  btServicio: {
    marginHorizontal: 20,
    marginVertical: 10

  },
  thumbServicio: {
    height: 120,
    width: 120,
    borderRadius: 120,
  },
  txtServicio: {
    textAlign: 'center'
  },
  workTitle: {
    backgroundColor: '#B7CECE'
  },
  workDesc: {
    flexDirection: 'row'
  },
  workDescTitle: {
    fontFamily: Fonts.content,
    fontWeight: 'bold',
    color: 'black'
  },
  workDescValue: {
    fontFamily: Fonts.content,
  },
  workViewBtn: {
    alignItems: 'center'
  },
  rowFront: {
    backgroundColor: '#FBB45F',
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    justifyContent: 'center',
  },
  rowBack: {
    alignItems: 'center',
    backgroundColor: 'red',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: 15,
  },
  backRightBtn: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  backRightIcon: {
    fontSize: Fonts.contentSize,
  },
  backRightBtnRight: {
    backgroundColor: 'red',
    right: 0,
  },
});


