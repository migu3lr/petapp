import { StyleSheet, Dimensions } from 'react-native';

const { width: WIDTH} = Dimensions.get('window')

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
  },
  title: {
    color: 'white',
    paddingVertical: 6,
    fontWeight: '400',
    fontSize: 25
  },
  icon: {
    color: 'white',
  },
  instrucciones: {
    alignSelf: 'flex-start',
  },
  instruccionesTxt: {
    textAlign: 'justify',
    color: 'white',
    paddingHorizontal: 30,
    paddingVertical: 10,
    fontSize: 16
  },
  Container: {
    flex:1,
    alignItems: 'center',
    justifyContent:'flex-start',

  },
  bgContainer: {
    flex: 1,
    width: null,
    height: null,
    alignItems: 'center',
  },
  logoContainer: {
    alignItems: 'center'
  },
  logo: {
    width: 120,
    height: 120
  },
  logoText: {
    color: 'white',
    fontSize: 40,
    fontWeight: '500',
    marginTop: 10,
    opacity: 0.8
  },
  inputContainerRow: {
    //flex:1,
    width: WIDTH -55,
    marginTop: 10,
    flexDirection:'row',
    justifyContent:'space-between',   
  },
  inputRow: {
    width: (WIDTH - 65)/2,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 10,
    backgroundColor: 'rgba(255,255,255, 0.5)',
    color: 'rgba(0,0,0, 0.8)',
  },
  inputCode: {
    width: WIDTH - 115,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(255,255,255, 0.5)',
    color: 'rgba(0,0,0, 0.8)',
  },
  inputContainerCode: {
    width: WIDTH - 100
  },
  inputIconCode: {
    position: 'absolute',
    top: 8,
    left: 15
  },
  inputContainer: {
    marginTop: 10,
  },
  input: {
    width: WIDTH - 55,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(255,255,255, 0.5)',
    color: 'rgba(0,0,0, 0.8)',
    marginHorizontal: 25
  },
  picker_wrapper: {
    width: WIDTH - 55,
    height: 45,
    borderRadius: 25,
    overflow: 'hidden',
  },
  picker: {
    height: 45,
    fontSize: 16,
    backgroundColor: 'rgba(255,255,255, 0.5)',
    color: 'rgba(0,0,0, 0.8)',
  },
  
  inputIcon: {
    position: 'absolute',
    top: 8,
    left: 37
  },
  btnEye: {
    position: 'absolute',
    top: 8,
    right: 37
  },
  btnResend: {
    width: 45,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#FBB45F',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnLogin: {
    width: WIDTH - 55,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#FBB45F',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20
  },  
  textLogin: {
    color: '#6F5F4E',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: '500',
  },
  optionLinksContainer: {
    width: WIDTH - 70,
    height: 60,
    justifyContent: 'center'
    
  },
  optionLink: {
    color: 'rgba(255,255,255,1)', 
    fontSize: 16
  },
  olLeft: {
    position: 'absolute',
    left: 0
  },
  olRight: {
    position: 'absolute',
    right: 0
  },
  authLinksContainer: {
    flexDirection: 'row',    
  },
  textAuthOp: {
    color: 'white', 
    textAlignVertical:'center'
  },
  desc: {
    color: 'white',
    fontSize: 14,
    textAlign:'justify'
  },
  descContainer: {
   // width: WIDTH - 70
  }
});

export default styles;
