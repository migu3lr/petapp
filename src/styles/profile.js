import { StyleSheet, Dimensions } from 'react-native';

const { width: WIDTH} = Dimensions.get('window')

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  title: {
    color: 'white',
    paddingVertical: 6,
    fontWeight: '400',
    fontSize: 25
  },
  icon: {
    color: 'white',
  },
  Container: {
    flex:1,

  },
  bgContainer: {
    flex: 1,
  },
  vwServicios: {
    flex:1,
    flexDirection:'column',
    justifyContent:'space-around',   
  },
  btServicio: {
    marginHorizontal: 20,
    marginVertical: 10,
    alignItems: 'center'
  },
  thumbServicio: {
    height: 180, 
    width: 180, 
    borderRadius: 120,
  },
  txtServicio: {
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20
  },
});

export default styles;
