Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
}

const compareDate = (dateParameter,dev=0) => {
    var compDate = new Date(new Date().setDate(new Date().getDate() + dev));
    return dateParameter.getDate() === compDate.getDate() && dateParameter.getMonth() === compDate.getMonth() && dateParameter.getFullYear() === compDate.getFullYear();
}

export const dealFormat = (dateParameter) => {
    if (compareDate(dateParameter)) return dateParameter.getHours().pad() + ":" + dateParameter.getMinutes().pad()
    else if (compareDate(dateParameter, -1)) return 'Ayer'
    else return parseInt(dateParameter.getDate()) + '/' + parseInt(dateParameter.getMonth())  + '/' + dateParameter.getFullYear().toString().substring(2)
}

export const chatFormat = (dateParameter) => {
    if (compareDate(dateParameter)) return dateParameter.getHours().pad() + ":" + dateParameter.getMinutes().pad()
    else if (compareDate(dateParameter, -1)) return 'Ayer ' + dateParameter.getHours().pad() + ":" + dateParameter.getMinutes().pad()
    else return parseInt(dateParameter.getDate()) 
        + '/' + parseInt(dateParameter.getMonth())  
        + '/' + dateParameter.getFullYear().toString().substring(2) 
        + ' ' + dateParameter.getHours().pad() + ":" + dateParameter.getMinutes().pad()
}

export const dealTitleFormat = (dateParameter) => {
    return parseInt(dateParameter.getDate()) + '/' + parseInt(dateParameter.getMonth()) 
}

export const workFormat = (dateParameter) => {
    return parseInt(dateParameter.getDate()) + '/' + parseInt(dateParameter.getMonth()) + '/' + dateParameter.getFullYear().toString().substring(2) 
}

export const inboxMsgFormat = (dateParameter) => {
    return parseInt(dateParameter.getDate()) + '/' + parseInt(dateParameter.getMonth())  + '/' + dateParameter.getFullYear().toString().substring(2) 
}
