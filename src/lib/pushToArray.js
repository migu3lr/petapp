export default pushToArray = (arr, id, obj) => {
    const index = arr.findIndex((e) => e[id] === obj[id]);

    if (index === -1) {
        arr.push(obj);
    } else {
        arr[index] = obj;
    }
}