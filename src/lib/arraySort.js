export default arraySort = (arr, by, sort='ASC') => {
  if (arr !== undefined && by !== undefined) {
    const newArray = [].concat(arr).slice().sort(function (a, b) {
      switch (sort) {
        case 'DESC':
          return b[by] - a[by]
          break;
        default:
          return a[by] - b[by]
          break;
      }
    })

    return newArray
  }
  return undefined
}