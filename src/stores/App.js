import { types, flow, getRoot } from 'mobx-state-tree' 

import { PermissionsAndroid } from 'react-native';
import Geolocation from 'react-native-geolocation-service';

import push from '../lib/pushToArray';
import sort from '../lib/arraySort';

import Works from './Works'
import WalkRoutes from './WalkRoutes'
import GeoLocations from './GeoLocations'
import Deals from './Deals'
import Inbox from './Inbox'
import Chats from './Chats'

import Pets from './Pets'
import Users from './Users'
import Locations from './Locations'

const App = types.model('App',{
    enterApp: false,
    LocationPermission: false,
    WatchingLocation: false,
    inbox: types.optional(types.array(Inbox),[]),
    works: types.optional(types.array(Works),[]),
    walkRoutes: types.optional(types.array(WalkRoutes),[]),
    geoLocations: types.optional(types.array(GeoLocations),[]),
    deals: types.optional(types.array(Deals),[]),
    chats: types.optional(types.array(Chats),[]),
    locations: types.optional(types.array(Locations),[]),
    pets: types.optional(types.array(Pets),[]),
    users: types.optional(types.array(Users),[]),
  })
  .actions(self => ({
    enterAppStatusChanged(enterApp){
      self.enterApp = enterApp

      const {authStore, iotStore} = getRoot(self)

      if (enterApp && authStore.loggedIn) {
        self.getAllData();
      }
    },
    updateData(base, list) {
      list.forEach(e => {
        if (base == 'users' && typeof e.address === 'string') e.address = JSON.parse(e.address)
        push(self[base],base == 'users'?'identityId':'id', e)
      });
      //console.warn(self[base])
    },
    deleteInboxMsg(id){
      const inbox_ix = self.inbox.findIndex((e) => e.id === id);
      self.inbox.splice(inbox_ix, 1);
    },
    getAllData: () => {
      const {iotStore, authStore} = getRoot(self)
      iotStore.pub(`srv/${authStore.user.profile}/get_all`, {});
    },
    deleteInboxData: () => {
      self.inbox = []
    },
    sendChatMessage: (message, type, work, deal) => {
      console.log(message, type, work, deal)
      let receiverId = ''
      let receiverProfile = ''

      if (type == 'work' && getRoot(self).authStore.user.profile == 'walker'){
        receiverId = work.identityId
        receiverProfile = 'customer'
      } 
      if (type == 'work' && getRoot(self).authStore.user.profile == 'customer'){
        receiverId = work.walkerId
        receiverProfile = 'walker'
      } 
      if (type == 'deal' && getRoot(self).authStore.user.profile == 'walker'){
        receiverId = deal.customerId
        receiverProfile = 'customer'
      } 
      if (type == 'deal' && getRoot(self).authStore.user.profile == 'customer'){
        receiverId = deal.walkerId
        receiverProfile = 'walker'
      } 

      const msg = {
        message,
        type,
        workId: work.id,
        dealId: deal.id,
        senderId: getRoot(self).authStore.user.identityId,
        senderProfile: getRoot(self).authStore.user.profile,
        receiverId,
        receiverProfile
      }
      console.log(msg)
      getRoot(self).iotStore.pub(`srv/chat/newMsg`, msg)
    },
    requestLocationPermission: flow(function* () {
      try {
        const granted = yield PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            'title': 'Example App',
            'message': 'Example App access to your location '
          }
        )
        self.LocationPermission = (granted === PermissionsAndroid.RESULTS.GRANTED)
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.warn("You can use the location")

        } else {
          console.warn("location permission denied")
        }
      } catch (err) {
        console.warn(err)
      }
      
    }),
    SendWalkRoutePosition: (position) => {
      const WalkRoutePosition = {
        walkRouteId: self.ActiveWalkRoute.id,
        walkerId: self.ActiveWalkRoute.walkerId,
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
      }
      console.warn(WalkRoutePosition)
      getRoot(self).iotStore.pub('srv/walkRoutes/newPosition',WalkRoutePosition)
    },
    WatchingLocationState: (state) => {
      self.WatchingLocation = state
    },
    watchLocation: flow(function* (action) {
      if (! self.LocationPermission) yield self.requestLocationPermission()
      if (self.LocationPermission && !self.WatchingLocation) {
        const  watchId = Geolocation.watchPosition(
          (position) => {
            if(!self.WatchingLocation) self.WatchingLocationState(true)
            console.log(position.coords.longitude, position.coords.latitude);

            switch (action) {
              case 'WORK_WATCH_POSITION':
                self.SendWalkRoutePosition(position)
                break;
      
            }    
          },
          (error) => {
            if(self.WatchingLocation) self.WatchingLocationState(false)
            console.log(error.code, error.message);
            alert("Por favor verifica el estado de tu GPS e intentalo nuevamente.")
          },
          { enableHighAccuracy: true, distanceFilter: 50 }
        )
        console.log(watchId)
      } else {console.log('No tiene permisos')}
    }),
    stopWatchingLocation: () => {
      if (self.WatchingLocation) {
        Geolocation.stopObserving()
        console.warn("watch ended")
        self.WatchingLocationState(false)
      }
    },

    actionFromServer: (action) => {
      switch (action) {
        case 'WORK_WATCH_POSITION':
          self.watchLocation(action)
          break;
        case 'STOP_WATCH_POSITION':
          self.stopWatchingLocation()
          break;

      }
    },
  }))
  .views(self =>({
    InboxbyID: (id) => {
      const msg = self.inbox.find((e) => e.id === id)
      return msg
    },
    ChatsbyType: (type) => {
      const chats = self.chats.filter((e) => e.type === type)
      return [].concat(chats)
    },
    ChatsbyDeal: (dealId) => {
      const chats = self.chats.filter((e) => e.dealId.id == dealId && e.type === 'deal')
      return sort(chats, 'sentAt')
    },
    ChatsbyWork: (workId) => {
      const chats = self.chats.filter((e) => e.workId.id == workId && e.type === 'work')
      return sort(chats, 'sentAt')
    },
    ChatbyIdType: (id, type) => {
      switch (type) {
        case 'deal':
          return self.ChatsbyDeal(id)
          break;
        case 'work':
          return self.ChatsbyWork(id)
          break;
        default:
          return []
          break;
      }
    },
    WorkbyDeal: (dealId) => {
      const { workId } = self.deals.find((e) => e.id === dealId)
      return workId
    },
    DealsbyWork: (workId) => self.deals.filter((e) => e.workId.id === workId),
    WorkbyID: (id) => self.works.find((e) => e.id === id),
    DealbyID: (id) => self.deals.find((e) => e.id === id),
    UserbyID: (id) => self.users.find((e) => e.identityId === id),
    UserRef: (identityId, profile) => getRoot(self).authStore.itsMe(identityId, profile) ? getRoot(self).authStore.user : self.UserbyID(identityId),
    PetbyID: (id) => self.pets.find((e) => e.id === id),
    PetRef: (id) => getRoot(self).authStore.user.profile == 'customer' ? getRoot(self).authStore.user.PetbyID(id) : self.PetbyID(id),
    LocationbyID: (id) => self.locations.find((e) => e.id === id),
    LocationRef: (id) => getRoot(self).authStore.user.profile == 'customer' ? getRoot(self).authStore.user.LocationbyID(id) : self.LocationbyID(id),
    GeoLocationsbyWork: (workId) => {
      const locations = self.geoLocations.filter((e) => e.walkRouteId.id === self.WorkbyID(workId).walkRouteId.id)
      if (locations !== undefined && locations.length > 0) {
        locations.sort(function (a, b) {
          return b.createdat - a.createdat;
        });
        return locations
      }
  
      return null
    },
    get ActiveWalkRoute() {return self.walkRoutes.find((e) => e.active)},
    get ActiveWork() {return self.works.find((e) => e.status === 'IN_PROGRESS')},
    get lastWorkGeolocation() {
      const locations = self.ActiveWork ? self.geoLocations.filter((e) => e.walkRouteId.id == self.ActiveWork.walkRouteId.id) : self.ActiveWork
      if (locations !== undefined && locations.length > 0){
        locations.sort(function (a, b) {
          return b.createdat - a.createdat;
        });
        return {
          latitude: locations[0].latitude,
          longitude: locations[0].longitude,
        }
      }
  
      return{
        latitude: 0,
        longitude: 0,
      }
      
    },
    get lastFinishedWork() {
      const works = self.works.filter((e) => e.status === 'FINISHED')
      if (works !== undefined && works.length > 0){
        works.sort(function (a, b) {
          return b.updatedat - a.updatedat;
        });
        return works[0]
      }

      return null
    }
  }))
    

export default App
