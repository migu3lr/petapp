import { types } from 'mobx-state-tree'


const Pets = types.model('Pets',{
    id: types.identifier,
    identityId: types.string,
    updatedat: types.number,
    age: types.string,
    name: types.string,
    breed: types.maybe(types.string),
    size: types.string,
    type: types.string
  })
  .actions(self => ({
  }))
    

export default Pets
