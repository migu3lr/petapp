import { types, flow, getRoot } from 'mobx-state-tree'

import Region from './Region'

const Locations = types.model('Locations',{
    id: types.identifier, 
    identityId: types.string,
    region: types.maybe(Region),
    address: types.string,
    address_details: types.optional(types.string, ''),
    address_name: types.string,
    updatedat: types.number
  })
    

export default Locations
