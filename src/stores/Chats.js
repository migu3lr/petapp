import { types, getRoot } from 'mobx-state-tree' 

import Works from './Works'
import Deals from './Deals'

const Chats = types.model('Chats',{
  id: types.identifier,  
  sentAt: types.Date, 
  senderId: types.string, 
  senderProfile: types.string,
  receiverId: types.string, 
  receiverProfile: types.string,
  workId: types.safeReference(Works),
  dealId: types.safeReference(Deals),
  status: types.string,
  type: types.string,
  message: types.string,
  attachment: types.maybe(types.string),
  attachmentType: types.maybe(types.string),
})
.views(self =>({
  get sender() {return getRoot(self).appStore.UserRef(self.senderId, self.senderProfile == 'srv' ? 'walker' : self.senderProfile)},
  get receiver() {return getRoot(self).appStore.UserRef(self.receiverId, self.receiverProfile)},
  get sender_name() {return self.senderProfile == 'srv' ? '[Eq. Soporte]' : self.sender.full_name},
  get msgText() {
    let str = self.message
    str = str.replace('#WalkerName', self.senderProfile == 'customer' ? '#CustomerName' : self.sender.full_name);
    str = str.replace('#CustomerName', self.senderProfile == 'customer' ? '#CustomerName' : self.receiver.given_name);
    return str
  },
}))


export default Chats
