import { types, flow, getRoot } from 'mobx-state-tree' 

import push from '../lib/pushToArray';
import sort from '../lib/arraySort';

import Works from './Works'

const Deals = types.model('Deals',{
  id: types.identifier,  
  createdat: types.Date,
  workId: types.reference(Works),
  customerId: types.string,
  walkerId: types.string,
  price: types.number,
  status: types.string
})
.actions(self => ({
  changeStatus: (status) => {
    const deal = {
      id: self.id,
      status,
      workId: self.workId.id,
      customerId: self.customerId,
      walkerId: self.walkerId
    }
    getRoot(self).iotStore.pub('srv/deals/changeStatus',deal)
  },
}))
.views(self =>({
  get walker() {return getRoot(self).appStore.UserRef(self.walkerId, 'walker')},
  get customer() {return getRoot(self).appStore.UserRef(self.customerId, 'customer')},
  get lastChatMsg() {
    const id = self.id
    const chats = getRoot(self).appStore.ChatsbyDeal(id)
    //console.warn(chats)
    if (chats !== undefined) {
      chats.sort(function (a, b) {
        return b.sentAt - a.sentAt;
      });
      return chats[0]
    }

    return null   
  }
}))
    

export default Deals
