import { types, flow, getRoot } from 'mobx-state-tree' 
import AsyncStorage from '@react-native-community/async-storage';
import * as Mobx from 'mobx'
import { Toast } from "native-base";

import * as ApiGateway from '../lib/api-gateway';
import * as Cognito from '../lib/aws-cognito';
import * as IoT from '../lib/aws-iot'; 
import wip from '../lib/wipEmpty';
import push from '../lib/pushToArray';

const Item = types.model('Item',{
  index: types.identifier,
  model: types.string,
  payload: types.frozen() 
})

const Transaction = types.model('Transactions',{
  id: types.identifier,  
  size: types.number,
  items: types.optional(types.array(Item),[])
})

const IotStore = types.model('IotStore',{
    connectPolicy: false,
    publicPublishPolicy: false,
    publicSubscribePolicy: false,
    publicReceivePolicy: false,
    deviceConnected: false,
    iotConnected: false,
    messageHandlerAttached: false,
    connectHandlerAttached: false,
    messageToSend: '',
    subscribedTopics: types.optional(types.array(types.string), []),
    transactions: types.optional(types.array(Transaction),[]),
  })
  .actions(self => ({
    CONNECT_POLICY_ATTACHED() {
      self.connectPolicy = true
    },
    PUBLIC_PUBLISH_POLICY_ATTACHED() {
      self.publicPublishPolicy = true
    },
    PUBLIC_SUBSCRIBE_POLICY_ATTACHED() {
      self.publicSubscribePolicy = true
    },
    PUBLIC_RECEIVE_POLICY_ATTACHED() {
      self.publicReceivePolicy = true
    },
    DEVICE_CONNECTED_STATUS_CHANGED(status) {
      self.deviceConnected = status
    },
    MESSAGE_HANDLER_ATTACHED(attached) {
      self.messageHandlerAttached = attached
    },
    CONNECT_HANDLER_ATTACHED(attached) {
      self.connectHandlerAttached = attached
    },
    LOGOUT() {
      self.connectPolicy = false
      self.publicPublishPolicy = false
      self.publicSubscribePolicy = false
      self.publicReceivePolicy = false
      self.deviceConnected = false
      self.iotConnected = false
      self.messageHandlerAttached = false
      self.connectHandlerAttached = false
      self.messageToSend = ''
      self.subscribedTopics = []
    },

    MESSAGE_TO_SEND_CHANGED(messageToSend) {
      self.messageToSend = messageToSend
    },
    ADD_SUBSCRIBED_TOPIC(topic) {
      self.subscribedTopics = [
        ...self.subscribedTopics,
        topic
      ]
    },
    CLEAR_SUBSCRIBED_TOPICS() {
      self.subscribedTopics = []
      //console.warn("Suscribed topics cleared.")
    },
    acquirePublicPolicies: flow(function* (connectCallback, closeCallback) {
      
      const loggedIn = yield Cognito.authUser();
      //getRoot(self).authStore.handleSignOut();
      //console.log('loggedIn', loggedIn)

      if (!loggedIn) {
        getRoot(self).authStore.handleSignOut();
        return Promise.resolve();
      }
      
      const identityId = Cognito.getIdentityId();
      getRoot(self).authStore.IDENTITY_UPDATED(identityId);
      getRoot(self).authStore.USER_UPDATED(JSON.parse(yield AsyncStorage.getItem('user')));
      const awsCredentials = JSON.parse(yield AsyncStorage.getItem('awsCredentials'));

      //console.warn('iot init')

      IoT.initNewClient(awsCredentials);
      IoT.attachConnectHandler(connectCallback);
      IoT.attachCloseHandler(closeCallback);
  
      if (!self.connectPolicy) {
        yield ApiGateway.attachConnectPolicy()
          self.CONNECT_POLICY_ATTACHED()
      }
  
      if (!self.publicPublishPolicy) {
        yield ApiGateway.attachPublicPublishPolicy()
          self.PUBLIC_PUBLISH_POLICY_ATTACHED()
      }
  
      if (!self.publicSubscribePolicy) {
        yield ApiGateway.attachPublicSubscribePolicy()
          self.PUBLIC_SUBSCRIBE_POLICY_ATTACHED()
      }
  
      if (!self.publicReceivePolicy) {
        yield ApiGateway.attachPublicReceivePolicy()
          self.PUBLIC_RECEIVE_POLICY_ATTACHED()
      }  
      /*
      while (!self.connectPolicy || !self.publicPublishPolicy || !self.publicSubscribePolicy || !self.publicReceivePolicy) {
        console.warn("connectPolicy:"+self.connectPolicy)
          console.warn("publicPublishPolicy:"+self.publicPublishPolicy)
          console.warn("publicSubscribePolicy:"+self.publicSubscribePolicy)
          console.warn("publicReceivePolicy:"+self.publicReceivePolicy)
          console.warn("deviceConnected:"+self.deviceConnected)
      }*/
    }),
    deviceConnectedStatusChanged : status => {
      self.DEVICE_CONNECTED_STATUS_CHANGED(status)
      if (status) self.attachConnectHandler()
    },
    handleDataUpdate: (base,payload) => {
      const { authStore, appStore } = getRoot(self)

      switch (base) {

        case 'pets':
        case 'locations':
          if (authStore.user.profile == 'customer') authStore.user.updateData(base,payload)
          if (authStore.user.profile == 'walker')  appStore.updateData(base,payload)
          Toast.show({text: `List of ${base} updated.`, type: "success", position: "top"})
          break;

        case 'users':
        case 'works':
        case 'walkRoutes':
        case 'geoLocations':
        case 'inbox':
        case 'deals':
        case 'chats':
          appStore.updateData(base,payload)
          Toast.show({text: `List of ${base} updated.`, type: "success", position: "top"})
          break; 

        case 'error':
          Toast.show({text: `${base}: ${payload.error}`, type: "danger", position: "top"})
          break;
        
        case 'action':
          appStore.actionFromServer(payload.action)
          Toast.show({text: `${base}: ${payload.action}`, position: "bottom"})
          break;

        default:
          console.warn('No action matched with message.')
          break;
      }
    },
    handleTransaction:(id,size,item) => {
      const i = self.transactions.findIndex((e) => e.id === id);

      if (i === -1) {
        self.transactions.push({
          id,
          size,
          items: [item]
        })
      } else {
        const i2 = self.transactions[i].items.findIndex((e) => e.index === item.index);

        if (i2 === -1) {
          self.transactions[i].items.push(item)
        } else {
          self.transactions[i].items[i2] = item
        }
      }

      //console.warn(Mobx.toJS(self.transactions))

      const trx_ix = self.transactions.findIndex((e) => e.id === id);
      const trx = self.transactions[trx_ix]
      if (trx.size == trx.items.length){
        for (index = 1; index <= trx.size; index++) { 

          const it = trx.items.find((e) => e.index == index)
          self.handleDataUpdate(it.model,it.payload)
        }
        self.transactions.splice(trx_ix, 1);
      }
      

    },
    newMessageReceived: (topic, payload) => {
      const { authStore, appStore } = getRoot(self)
      const topic_split = topic.split('/')

      if (topic_split[1] == authStore.identityId) {
        //console.log(topic,payload)
        switch (topic_split[2]) {
          case 'TRX':
            self.handleTransaction(topic_split[3],parseInt(topic_split[4]),{index: topic_split[5], model: topic_split[6], payload})
            break;

          default:
            self.handleDataUpdate(topic_split[2],payload)
            break;
        }
      }
    },
    attachMessageHandler : () => {
      if (!self.messageHandlerAttached) {
        IoT.attachMessageHandler((topic, jsonPayload) => {
          const payload = JSON.parse(jsonPayload.toString())
          //console.warn('RECEIVED:', topic, payload)
          self.newMessageReceived(topic, payload)
        })
      }
      self.MESSAGE_HANDLER_ATTACHED(true)
    },
    attachConnectHandler : () => {
      //console.warn("attachConnectHandler:")
      if (!self.connectHandlerAttached) {
        
          /*console.warn("connectPolicy:"+self.connectPolicy)
          console.warn("publicPublishPolicy:"+self.publicPublishPolicy)
          console.warn("publicSubscribePolicy:"+self.publicSubscribePolicy)
          console.warn("publicReceivePolicy:"+self.publicReceivePolicy)
          console.warn("deviceConnected:"+self.deviceConnected)*/
          IoT.unsubscribeFromTopics(self.subscribedTopics);          
          self.attachMessageHandler()        
          self.subscribeToTopic('srv/notifications/#')
          self.subscribeToTopic(`srv/${getRoot(self).authStore.identityId}/#`)
          getRoot(self).appStore.enterAppStatusChanged(true);
          //console.warn('IoT initiated');
        
      }
      self.CONNECT_HANDLER_ATTACHED(true)
    },
    messageToSendChanged : messageToSend => {
      self.MESSAGE_TO_SEND_CHANGED(messageToSend)
    },
    subscribeToTopic : topic => {
      if (self.subscribedTopics.includes(topic)) {
        console.warn('Already subscribed to topic', topic);
      } else {
        IoT.subscribe(topic);
        self.ADD_SUBSCRIBED_TOPIC(topic);
      }
    },
    pub: (topic,payload) => {
      const { identityId } = getRoot(self).authStore
      const t = `${topic}/${identityId}`;
      IoT.publish(t, JSON.stringify(wip(payload)));
    }
    

  }))
    

export default IotStore