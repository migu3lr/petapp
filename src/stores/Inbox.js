import { types, flow, getRoot } from 'mobx-state-tree' 

import push from '../lib/pushToArray';
import sort from '../lib/arraySort';

import Works from './Works'
import Deals from './Deals'

const Inbox = types.model('Inbox',{
  id: types.identifier,  
  createdat: types.Date, 
  identityId: types.string,
  workId: types.reference(Works),
  dealId: types.maybe(types.reference(Deals)),
  sender: types.string,
  status: types.string,
  type: types.string    
})
.views(self =>({
  get customer_name() {return self.workId.customer.full_name},
  get work_type() {return self.workId.work_type},
  get service_type() {return self.workId.service_type}
}))
    

export default Inbox
