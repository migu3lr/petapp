import { types } from 'mobx-state-tree'

import IotStore from './Iot'
import AuthStore from './Auth'
import AppStore from './App'
import FormsStore from './Forms'
import VisualOptionsStore from './VisualOptions'


const RootStore = types.model('RootStore',{
    iotStore: types.optional(IotStore,{}),
    authStore: types.optional(AuthStore,{}),
    appStore: types.optional(AppStore,{}),
    formsStore: types.optional(FormsStore,{}),
    visualOptionsStore: types.optional(VisualOptionsStore,{}),
  })
   
export default RootStore