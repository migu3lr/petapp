import AS_Walk from 'petapp/src/components/AS_Walk'

export default services = [
    {
        name: 'Hospedaje',
        thumb: require('petapp/src/images/pethost.jpg'),
        action: AS_Walk
    },
    {
        name: 'Paseo de Perros',
        thumb: require('petapp/src/images/dogwalking.jpg'),
        action: AS_Walk
    },
    {
        name: 'Guardería',
        thumb: require('petapp/src/images/pethost2.jpg'),
        action: AS_Walk
    },
    {
        name: 'Peluquería',
        thumb: require('petapp/src/images/pethair2.jpg'),
        action: AS_Walk
    },
]