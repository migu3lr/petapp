import { types, flow, getRoot } from 'mobx-state-tree'

import Pets from './Pets'
import Locations from './Locations'

const address_cognito = types.model('address_cognito',{
  'country': types.maybe(types.string)
})

const User = types.model('User',{
    identityId: types.string,  
    created_at: types.number, 
    pets: types.maybe(types.optional(types.array(Pets),[])),
    locations: types.maybe(types.optional(types.array(Locations),[])),

    'email_verified': types.maybe(types.string),
    'username':       types.maybe(types.string),
    'sub':            types.maybe(types.string),
	  'profile':        types.maybe(types.string),
    'email':	        types.maybe(types.string),
    'given_name':	    types.maybe(types.string),
    'family_name':	  types.maybe(types.string),
    'locale':         types.maybe(types.string),
    'zoneinfo':       types.maybe(types.string),
    'updated_at':     types.maybe(types.string),
    'address':        types.maybe(address_cognito),    	
    'custom:doc_num': types.optional(types.string, ''),
	  'custom:doc_type':types.optional(types.string, ''),
    'preferred_username': types.optional(types.string, ''),
    'birthdate':      types.optional(types.string, ''),
    'picture':        types.optional(types.string, ''),
    
  })
  .actions(self => ({
    updateData(base, list) {
      if (base == 'pets' && getRoot(self).authStore.showPetReg === undefined) getRoot(self).authStore.PetReg_Visible(list.length == 0)

      self[base] = list;
    }
  }))
  .views(self =>({
    get full_name() {return self.given_name + ' ' + self.family_name},
    PetbyID: (id) => {
      const pet = self.pets.find((e) => e.id === id)
      return pet
    },
    LocationbyID: (id) => {
      const location = self.locations.find((e) => e.id === id)
      return location
    }
  }))
    

export default User
