import { types } from 'mobx-state-tree'

const Inbox = types.model('Inbox', {
  filter: types.optional(types.string, 'FILTER_ALL')
})
  .actions(self => ({
    updateFilter(filter) {
      if (self.filter != filter) {
        self.filter = filter
      }
    },
  }))

const VisualOptionsStore = types.model('VisualOptionsStore', {
  Inbox: types.optional(Inbox,{filter: 'FILTER_ALL'}),
})




export default VisualOptionsStore