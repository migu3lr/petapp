import { types, flow, getRoot } from 'mobx-state-tree' 


const WalkRoutes = types.model('WalkRoutes',{
  id: types.identifier,  
  startedat: types.Date,
  finishedat: types.maybe(types.Date),
  isActive: types.string,
  walkerId: types.string
})
.actions(self => ({
  
}))
.views(self =>({
  get walker() {return getRoot(self).appStore.UserRef(self.walkerId, 'walker')},
  get active() {return (self.isActive == 'true')}
}))


export default WalkRoutes
