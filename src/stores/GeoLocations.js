import { types, flow, getRoot } from 'mobx-state-tree' 

import WalkRoutes from './WalkRoutes'

const GeoLocations = types.model('GeoLocations',{
  id: types.identifier,  
  createdat: types.Date,
  walkRouteId: types.reference(WalkRoutes),
  walkerId: types.maybe(types.string),
  latitude: types.number,
  longitude: types.number
})
.actions(self => ({
  
}))
.views(self =>({
  get walker() {return getRoot(self).appStore.UserRef(self.walkerId, 'walker')},
}))


export default GeoLocations
