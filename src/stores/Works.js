import { types, flow, getRoot } from 'mobx-state-tree' 

import { Alert } from "react-native";

import WalkRoutes from './WalkRoutes'

const Works = types.model('Works',{
  id: types.identifier,  
  createdat: types.Date,
  identityId: types.string,
  walkerId: types.maybe(types.string),
  locationId: types.string,
  petId: types.string,
  status: types.string,
  workType: types.string,
  walkNow: types.boolean,
  walkType: types.string,
  walkRouteId: types.safeReference(WalkRoutes),
  rating: types.maybe(types.number),
})
.actions(self => ({
  deal: () => {
    const work = {
      id: self.id,
      price: 10000
    }
    getRoot(self).iotStore.pub('srv/works/deal',work)
  },
  changeStatus: (status) => {
    const work = {
      id: self.id,
      status,
      customerId: self.identityId,
      walkerId: self.walkerId
    }
    getRoot(self).iotStore.pub('srv/works/changeStatus',work)
  },
  start: () => {
    const work = {
      id: self.id,
      customerId: self.identityId,
      walkerId: self.walkerId
    }

    let confirm = (getRoot(self).appStore.ActiveWalkRoute === undefined)
    if (getRoot(self).appStore.ActiveWalkRoute !== undefined){
      Alert.alert(
        'Ruta de paseo activa',
        'Parece que tienes una ruta de paseo activa en este momento, deseas agregar este trabajo a la ruta actual?',
        [
          {
            text: 'Cancelar',
            onPress: () => { 
               console.warn('Cancel Pressed')
            },
            style: 'cancel',
          },
          {text: 'OK', onPress: () => {
            getRoot(self).iotStore.pub('srv/works/startWork',work)
          }},
        ],
        {cancelable: false},
      )
    } else {

      getRoot(self).iotStore.pub('srv/works/startWork',work)
    }

    
  },
  finish:  () => {
    const work = {
      id: self.id,
      customerId: self.identityId,
      walkerId: self.walkerId
    }

    getRoot(self).iotStore.pub('srv/works/finishWork',work)
  },
  rate:  (rating) => {
    const work = {
      id: self.id,
      rating,
      customerId: self.identityId,
      walkerId: self.walkerId
    }

    getRoot(self).iotStore.pub('srv/works/rateWork',work)
  }
}))
.views(self =>({
  get walker() {return getRoot(self).appStore.UserRef(self.walkerId, 'walker')},
  get customer() {return getRoot(self).appStore.UserRef(self.identityId, 'customer')},
  get pet() {return getRoot(self).appStore.PetRef(self.petId)},
  get location() {return getRoot(self).appStore.LocationRef(self.locationId)},
  get work_type() {return (self.workType == 'walk' ? 'PASEO':'HOSPEDAJE') + (self.walkNow ? ' INMEDIATO':'')},
  get service_type() {return (self.walkType || 'N/A')},
}))


export default Works
