import { types, flow, getRoot } from 'mobx-state-tree'

const address_cognito = types.model('address_cognito',{
  'country': types.maybe(types.string)
})

const Users = types.model('Users',{
    identityId: types.identifier,
    address:  types.maybe(address_cognito),
    created_at: types.number,
    email: types.string,
    family_name: types.string,
    given_name: types.string,
    locale: types.string,
    profile: types.string,
    prom: types.maybe(types.string),
    rates5: types.maybe(types.number),
    rates4: types.maybe(types.number),
    rates3: types.maybe(types.number),
    rates2: types.maybe(types.number),
    rates1: types.maybe(types.number),
  })
  .views(self =>({
    get full_name() {return self.given_name + ' ' + self.family_name},
    get prom_rate() {return parseFloat(self.prom)}
  }))

export default Users

