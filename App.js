
import React, {Component} from 'react';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import {Provider} from 'mobx-react'

import makeInspectable from 'mobx-devtools-mst';
import { connectToDevTools } from 'mobx-devtools/lib/mobxDevtoolsBackend';

import { Root } from "native-base";
import AsyncStorage from '@react-native-community/async-storage';
import RootStore from './src/stores/Root'
import MainNav from './src/navs/Main';
import CoMainNav from './src/navs/CoMain';
import AuthNav from './src/navs/Auth';
import AuthLoadingScreen from './src/navs/_AuthLoading';

AsyncStorage.clear();



const store = RootStore.create({})

connectToDevTools({ host: '192.168.0.17', port: '8098' });
makeInspectable(store);



let Navigation = createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: MainNav,
    CoApp: CoMainNav,
    Auth: AuthNav
  },
  {
    initialRouteName: 'AuthLoading'
  }
))

export default class App extends Component {
  render() {
    return (
      <Provider 
        appStore={store.appStore} 
        iotStore={store.iotStore} 
        authStore={store.authStore} 
        forms={store.formsStore}
        vOpts= {store.visualOptionsStore} 
      >
        <Root>
          <Navigation />
        </Root>
      </Provider>
    );
  }
}



