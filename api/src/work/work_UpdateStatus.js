/*
  Copyright 2017-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.

  Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except
  in compliance with the License. A copy of the License is located at

      http://aws.amazon.com/apache2.0/

  or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
  specific language governing permissions and limitations under the License.
*/

import { v4 as uuid } from 'uuid';
import * as dynamodb from '../helpers/dynamodb';
import * as iot from '../helpers/aws-iot';
import { success, failure } from '../helpers/response';

export const main = async (event, context, callback) => {

  console.log(event)

  const queryParams_Works = {
    TableName: 'Works',
    KeyConditionExpression: "id = :id",
    ExpressionAttributeValues: {
      ":id": event.id
    }    
  };  


  const updateParams_Works = {
    TableName: 'Works',
    Key: { id : event.id },
    UpdateExpression: 'set #status = :status , #updatedat = :updatedat',
    ExpressionAttributeNames: {'#status': 'status', '#updatedat': 'updatedat'},
    ExpressionAttributeValues: {
      ':status' : event.status,
      ':updatedat' : event.updatedat
    }
  }; 

  try {
    await dynamodb.call('update', updateParams_Works);
    const work = await dynamodb.call('query', queryParams_Works);
    if (work.Items.length > 0) {
      const w = work.Items[0]
      iot.publish(`srv/${event.customerId}/works`, [w], callback)
      iot.publish(`srv/${event.walkerId}/works`, [w], callback)
    }

    callback(null, success(work));
   
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false, error: e }));
  }
};
