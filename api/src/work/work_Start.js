/*
  Copyright 2017-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.

  Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except
  in compliance with the License. A copy of the License is located at

      http://aws.amazon.com/apache2.0/

  or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
  specific language governing permissions and limitations under the License.
*/

import { v1 as uuid } from 'uuid';
import * as dynamodb from '../helpers/dynamodb';
import * as iot from '../helpers/aws-iot';
import { success, failure } from '../helpers/response';

export const main = async (event, context, callback) => {

  console.log(event)

  const queryParams_Works = {
    TableName: 'Works',
    KeyConditionExpression: "id = :id",
    ExpressionAttributeValues: {
      ":id": event.id
    }    
  };  

  const queryParams_WalkRoutes = (id) =>({
    TableName: 'WalkRoutes',
    KeyConditionExpression: "id = :id",
    ExpressionAttributeValues: {
      ":id": id
    }    
  }); 

  const queryParams_WalkRoutes_isActive = {
    TableName: 'WalkRoutes',
    IndexName: 'walkerId_isActive_ix',
    KeyConditionExpression: "walkerId = :walkerId and isActive = :true",
    ExpressionAttributeValues: {
      ":walkerId": event.walkerId,
      ':true': 'true'
    }    
  }; 

  const updateParams_Works = (WalkRouteId) => ({
    TableName: 'Works',
    Key: { id : event.id },
    UpdateExpression: 'set #status = :in_progress , #updatedat = :updatedat, #walkRouteId = :walkRouteId',
    ExpressionAttributeNames: {'#status': 'status', '#updatedat': 'updatedat', '#walkRouteId' : 'walkRouteId'},
    ExpressionAttributeValues: {
      ':in_progress' : 'IN_PROGRESS',
      ':updatedat' : event.updatedat,
      ':walkRouteId' : WalkRouteId
    }
  }); 

  const putParams_WalkRoutes = (id) => ({
    TableName: 'WalkRoutes',
    Item: {
      id,
      startedat: event.updatedat,
      walkerId: event.walkerId,
      isActive: 'true'
    },
  }) 

  const putParams_WorkActivity = {
    TableName: 'WorkActivity',
    Item: {
      id: uuid(),
      workId: event.id,
      createdat: event.updatedat,
      status: 'IN_PROGRESS'
    },
  } 

  let WalkRouteId = uuid()

  try {
    const work = await dynamodb.call('query', queryParams_Works);
    const w = work.Items[0]
    console.log('Validar si el trabajo esta confirmado')
    if (w.status != 'CONFIRMED') {
      iot.publish(`srv/${event.walkerId}/error`, {
        error: 'WORK_NOT_CONFIRMED'
      }, callback)
    } else {
      console.log('Trabajo confirmado continua, validar si hay una ruta activa')
      const WalkRoutes_isActive = await dynamodb.call('query', queryParams_WalkRoutes_isActive);
      if (WalkRoutes_isActive.Items.length > 0){
        console.log('Existe ruta activa')
        WalkRouteId = WalkRoutes_isActive.Items[0].id
        console.log('actualiza trabajo con la ruta activa')
        await dynamodb.call('update', updateParams_Works(WalkRouteId));
        console.log('inserta registro de actividad')
        await dynamodb.call('put', putParams_WorkActivity);
      } else {
        console.log('no hay una ruta activa, entonces se crea ruta')
        await dynamodb.call('put', putParams_WalkRoutes(WalkRouteId));
        console.log('actualiza trabajo con la ruta nueva')
        await dynamodb.call('update', updateParams_Works(WalkRouteId));
        console.log('inserta registro de actividad')
        await dynamodb.call('put', putParams_WorkActivity);
      }

      const _work = await dynamodb.call('query', queryParams_Works);
      const _walkRoute = await dynamodb.call('query', queryParams_WalkRoutes(WalkRouteId));

      const TRX_ID = uuid();
      const TRX_SIZE_CUSTOMER = 2
      const TRX_SIZE_WALKER = 3
      let TRX_IX = 0
      iot.publish(`srv/${event.walkerId}/TRX/${TRX_ID}/${TRX_SIZE_WALKER}/${++TRX_IX}/action`, {action: 'WORK_WATCH_POSITION'}, callback)
      
      iot.publish(`srv/${event.customerId}/TRX/${TRX_ID}/${TRX_SIZE_CUSTOMER}/${TRX_IX}/walkRoutes`, _walkRoute.Items, callback)
      iot.publish(`srv/${event.walkerId}/TRX/${TRX_ID}/${TRX_SIZE_WALKER}/${++TRX_IX}/walkRoutes`, _walkRoute.Items, callback)
      
      iot.publish(`srv/${event.customerId}/TRX/${TRX_ID}/${TRX_SIZE_CUSTOMER}/${TRX_IX}/works`, _work.Items, callback)
      iot.publish(`srv/${event.walkerId}/TRX/${TRX_ID}/${TRX_SIZE_WALKER}/${++TRX_IX}/works`, _work.Items, callback)
      

      
    }

    callback(null, success(work));
   
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false, error: e }));
  }
};
