/*
  Copyright 2017-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.

  Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except
  in compliance with the License. A copy of the License is located at

      http://aws.amazon.com/apache2.0/

  or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
  specific language governing permissions and limitations under the License.
*/

import { v4 as uuid } from 'uuid';
import * as dynamodb from '../helpers/dynamodb';
import * as iot from '../helpers/aws-iot';
import { success, failure } from '../helpers/response';

export const main = async (event, context, callback) => {

  console.log(event)

  const queryParams_Works = {
    TableName: 'Works',
    KeyConditionExpression: "id = :id",
    ExpressionAttributeValues: {
      ":id": event.workId
    }    
  };  

  const putParams_Deals = (customerId) => ({
    TableName: 'Deals',
    Item: {
      ...event,
      customerId
    },
  }); 

  try {
    const work = await dynamodb.call('query', queryParams_Works);
    if (work.Items.length > 0) {
      const w = work.Items[0]

      if (!w.walkerId || w.walkerId == '' || w.status == 'PENDING') {
        await dynamodb.call('put', putParams_Deals(w.identityId));
        iot.publish(`srv/${event.walkerId}/deals`, [{...event, customerId: w.identityId}], callback)
        iot.publish(`srv/${w.identityId}/deals`, [{...event, customerId: w.identityId}], callback)

        //Enviar informacion  de Inbox
        const payloadInbox = {
          identityId: w.identityId,
          sender: event.walkerId,
          type: 'deal',
          workId: event.workId,
          dealId: event.id
        }
        iot.publish('srv/inbox/newMsg', payloadInbox, callback)  

        //Enviar informacion  de Chat
        const payloadChat1 = {
          receiverId: w.identityId,
          receiverProfile: 'customer',
          senderId: event.walkerId,
          senderProfile: 'walker',
          type: 'deal',
          workId: event.workId,
          dealId: event.id,
          message: 'Hola #CustomerName, me gustaría ayudarte con tu solicitud. Revisa mi perfil si quieres mas información sobre mi: #WalkerInfoLink'
        }
        iot.publish('srv/chat/newMsg/srv', payloadChat1, callback)  

        //Enviar informacion  de Chat
        const payloadChat2 = {
          receiverId: w.identityId,
          receiverProfile: 'customer',
          senderId: event.walkerId,
          senderProfile: 'srv',
          type: 'deal',
          workId: event.workId,
          dealId: event.id,
          message: 'Habla con #WalkerName y al finalizar haz clic en CONFIRMAR si le consideras un buen candidato para el servicio. '
          + 'El pago que realices en ese momento es 100% reembolsable si el trabajo no se realiza y solo se entregará al proveedor hasta que complete el trabajo solicitado para garantizar su calidad.'
        }
        iot.publish('srv/chat/newMsg/srv', payloadChat2, callback)  

        callback(null, success(payloadChat1));
      }
    }
    
    
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false, error: e }));
  }
};
