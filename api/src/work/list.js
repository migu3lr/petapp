/*
  Copyright 2017-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.

  Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except
  in compliance with the License. A copy of the License is located at

      http://aws.amazon.com/apache2.0/

  or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
  specific language governing permissions and limitations under the License.
*/

import { v4 as uuid } from 'uuid';
import * as dynamodb from '../helpers/dynamodb';
import * as iot from '../helpers/aws-iot';
import { success, failure } from '../helpers/response';
import { pushToArray } from '../helpers/pushToArray';
import { asyncForEach } from '../helpers/asyncForEach';

export const main = async (event, context, callback) => {

  const queryParams_GeoLocations = (WalkRouteId) => ({
    TableName: 'GeoLocations',
    IndexName: 'walkRouteId_ix',
    KeyConditionExpression: "walkRouteId = :walkRouteId",
    ExpressionAttributeValues: {
      ":walkRouteId": WalkRouteId
    }
  })

  const queryParams_WalkRoutes = (WalkRouteId) => ({
    TableName: 'WalkRoutes',
    KeyConditionExpression: "id = :id",
    ExpressionAttributeValues: {
      ":id": WalkRouteId
    }    
  })
  
  const queryParams_Chats = (workId) => ({
    TableName: 'Chats',
    IndexName: 'workId_ix',
    KeyConditionExpression: 'workId = :workId',
    ExpressionAttributeValues: {
      ':workId': workId,
    },
  });

  const queryParams_WorksWalker = {
    TableName: 'Works',
    IndexName: 'walkerId_ix',
    KeyConditionExpression: 'walkerId = :walkerId',
    ExpressionAttributeValues: {
      ':walkerId': event.identityId,
    },
  };

  const queryParams_DealsCustomer = {
    TableName: 'Deals',
    IndexName: 'customerId_ix',
    KeyConditionExpression: 'customerId = :customerId',
    ExpressionAttributeValues: {
      ':customerId': event.identityId,
    },
  };

  const queryParams_DealsWalker = {
    TableName: 'Deals',
    IndexName: 'walkerId_ix',
    KeyConditionExpression: 'walkerId = :walkerId',
    ExpressionAttributeValues: {
      ':walkerId': event.identityId,
    },
  };

  const queryParams_Users = (identityId) => ({
    TableName: 'Users',
    KeyConditionExpression: 'identityId = :identityId',
    ExpressionAttributeValues: {
      ':identityId': identityId,
    }
  });

  const queryParams_Pets = (identityId) => ({
    TableName: 'Pets',
    IndexName: 'identityId_ix',
    KeyConditionExpression: 'identityId = :identityId',
    ExpressionAttributeValues: {
      ':identityId': identityId,
    },
  });

  const queryParams_Locations = (identityId) => ({
    TableName: 'Locations',
    IndexName: 'identityId_ix',
    KeyConditionExpression: 'identityId = :identityId',
    ExpressionAttributeValues: {
      ':identityId': identityId,
    },
  });

  let listUsers = []
  let listWorks = []

  let lisWalkRoutes = []

  const TRX_ID = uuid();
  const TRX_SIZE = (event.profile == 'walker' ? 8 : 6)
  
  try {
    const Deals = await dynamodb.call('query', event.profile == 'customer' ? queryParams_DealsCustomer : queryParams_DealsWalker);
    Deals.Items.forEach(e => pushToArray(listWorks, e.workId)); //Listar Trabajos relacionados

    const w = await dynamodb.call('scan', {TableName: 'Works'});
    const Works = w.Items.filter(e => (listWorks.findIndex((i) => i === e.id) !== -1) || e.identityId == event.identityId || e.walkerId == event.identityId ) //Filtrar trabajos relacionados a enviar
    
    Works.forEach(e => pushToArray(listUsers, event.profile == 'customer' ? e.walkerId : e.identityId));
    Deals.Items.forEach(e => pushToArray(listUsers, event.profile == 'customer' ? e.walkerId : e.customerId));
    
    Works.forEach(e => pushToArray(lisWalkRoutes, e.walkRouteId));

    //Enviar informcion de Usuarios
    let Users = []
    let Pets = []
    let Locations = []
    const start1 = async () => {
      await asyncForEach(listUsers, async (e) => {
        const _User = await dynamodb.call('query', queryParams_Users(e));
        Users = Users.concat(_User.Items)

        if (event.profile == 'walker') {        
          const _Pets = await dynamodb.call('query', queryParams_Pets(e));
          Pets = Pets.concat(_Pets.Items) 
          
          const _Locations = await dynamodb.call('query', queryParams_Locations(e));
          Locations = Locations.concat(_Locations.Items) 
        }      
      });
    }   

    let Chats = []
    const start2 = async () => {
      await asyncForEach(Works, async (e) => {
        const _Chats = await dynamodb.call('query', queryParams_Chats(e.id));
        Chats = Chats.concat(_Chats.Items)
      });
    }

    let WalkRoutes = []
    let GeoLocations = []
    const start3 = async () => {
      await asyncForEach(lisWalkRoutes, async (e) => {
        const _WalkRoutes = await dynamodb.call('query', queryParams_WalkRoutes(e));
        WalkRoutes = WalkRoutes.concat(_WalkRoutes.Items)

        const _GeoLocations = await dynamodb.call('query', queryParams_GeoLocations(e));
        GeoLocations = GeoLocations.concat(_GeoLocations.Items)
      });
    }

    
    
   

    let TRX_IX = 0

    await start1()
    iot.publish(`srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/${++TRX_IX}/users`, Users, callback)  

    if (event.profile == 'walker') {
      iot.publish(`srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/${++TRX_IX}/pets`, Pets, callback)
      iot.publish(`srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/${++TRX_IX}/locations`, Locations, callback)
    }

    await start3()
    iot.publish(`srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/${++TRX_IX}/walkRoutes`, WalkRoutes, callback)
    iot.publish(`srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/${++TRX_IX}/geoLocations`, GeoLocations, callback)

    //Enviar informacion de Trabajos
    iot.publish(`srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/${++TRX_IX}/works`, Works, callback)
    console.log('WORKS: ', Works)

    iot.publish(`srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/${++TRX_IX}/deals`, Deals.Items, callback)
    console.log('DEALS: ', Deals)

    await start2()
    iot.publish(`srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/${++TRX_IX}/chats`, Chats, callback)
    console.log('CHATS: ', Chats)

    callback(null, success(Works));
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false, error: e }));
  }
};
