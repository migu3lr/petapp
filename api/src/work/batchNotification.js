/*
  Copyright 2017-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.

  Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except
  in compliance with the License. A copy of the License is located at

      http://aws.amazon.com/apache2.0/

  or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
  specific language governing permissions and limitations under the License.
*/

import { v4 as uuid } from 'uuid';
import * as dynamodb from '../helpers/dynamodb';
import * as iot from '../helpers/aws-iot';
import { success, failure } from '../helpers/response';

export const main = async (event, context, callback) => {

  console.log(event)

  const params = {
    TableName: 'Users',
    IndexName: 'profile_ix',
    KeyConditionExpression: "profile = :profile",
    ExpressionAttributeValues: {
      ":profile": "walker"
    }    
  };  

  const queryParams_Users = (identityId) => ({
    TableName: 'Users',
    KeyConditionExpression: 'identityId = :identityId',
    ExpressionAttributeValues: {
      ':identityId': identityId,
    }
  });

  const queryParams_Pets = (identityId) => ({
    TableName: 'Pets',
    IndexName: 'identityId_ix',
    KeyConditionExpression: 'identityId = :identityId',
    ExpressionAttributeValues: {
      ':identityId': identityId,
    },
  });

  const queryParams_Locations = (identityId) => ({
    TableName: 'Locations',
    IndexName: 'identityId_ix',
    KeyConditionExpression: 'identityId = :identityId',
    ExpressionAttributeValues: {
      ':identityId': identityId,
    },
  });

  const TRX_ID = uuid();
  const TRX_SIZE = 4

  try {
    const walkers = await dynamodb.call('query', params);
    walkers.Items.forEach(async walker => {

      //Enviar informacion de Usuarios
      const users = await dynamodb.call('query', queryParams_Users(event.identityId));
      users.Items.length > 0 && iot.publish(`srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/1/users`, users.Items, callback)
   
      const pets = await dynamodb.call('query', queryParams_Pets(event.identityId));
      pets.Items.length > 0 && iot.publish(`srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/2/pets`, pets.Items, callback)

      const locations = await dynamodb.call('query', queryParams_Locations(event.identityId));
      locations.Items.length > 0 && iot.publish(`srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/3/locations`, locations.Items, callback)

      //Enviar informacion de Trabajos
      const topic1 = `srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/4/works`
      iot.publish(topic1, [event], callback)

      //Enviar informacion  de Inbox
      const topic2 = `srv/inbox/newMsg`
      const payload = {
        identityId: walker.identityId,
        sender: event.identityId,
        type: 'work',
        workId: event.id
      }
      iot.publish(topic2, payload, callback)  

    });
    callback(null, success(payload));
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false, error: e }));
  }
};
