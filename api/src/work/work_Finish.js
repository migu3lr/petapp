/*
  Copyright 2017-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.

  Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except
  in compliance with the License. A copy of the License is located at

      http://aws.amazon.com/apache2.0/

  or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
  specific language governing permissions and limitations under the License.
*/

import { v1 as uuid } from 'uuid';
import * as dynamodb from '../helpers/dynamodb';
import * as iot from '../helpers/aws-iot';
import { success, failure } from '../helpers/response';

export const main = async (event, context, callback) => {

  console.log(event)

  const queryParams_Works = {
    TableName: 'Works',
    KeyConditionExpression: "id = :id",
    ExpressionAttributeValues: {
      ":id": event.id
    }    
  };  

  
  const queryParams_Works_WalkRoutes_InProgress = (WalkRouteId) =>({
    TableName: 'Works',
    IndexName: 'walkRouteId_status_ix',
    KeyConditionExpression: "walkRouteId = :WalkRouteId and #status = :in_progress",
    ExpressionAttributeNames: {'#status': 'status'},
    ExpressionAttributeValues: {
      ":WalkRouteId": WalkRouteId,
      ':in_progress': 'IN_PROGRESS'
    }    
  }); 

  const queryParams_WalkRoutes = (id) =>({
    TableName: 'WalkRoutes',
    KeyConditionExpression: "id = :id",
    ExpressionAttributeValues: {
      ":id": id
    }    
  }); 

  const queryParams_WalkRoutes_isActive = {
    TableName: 'WalkRoutes',
    IndexName: 'walkerId_isActive_ix',
    KeyConditionExpression: "walkerId = :walkerId and isActive = :true",
    ExpressionAttributeValues: {
      ":walkerId": event.walkerId,
      ':true': 'true'
    }    
  }; 

  const updateParams_Works = {
    TableName: 'Works',
    Key: { id : event.id },
    UpdateExpression: 'set #status = :finished , updatedat = :updatedat',
    ExpressionAttributeNames: {'#status': 'status'},
    ExpressionAttributeValues: {
      ':finished' : 'FINISHED',
      ':updatedat' : event.updatedat,
    }
  }; 

  const updateParams_WalkRoutes = (WalkRouteId) => ({
    TableName: 'WalkRoutes',
    Key: { id : WalkRouteId },
    UpdateExpression: 'set isActive = :false , finishedat = :finishedat',
    ExpressionAttributeValues: {
      ':false' : 'false',
      ':finishedat' : event.updatedat,
    }
  }); 

  const putParams_WorkActivity = {
    TableName: 'WorkActivity',
    Item: {
      id: uuid(),
      workId: event.id,
      createdat: event.updatedat,
      status: 'FINISHED'
    },
  } 


  try {
    const work = await dynamodb.call('query', queryParams_Works);
    const w = work.Items[0]
    console.log('Validar si el trabajo esta en Progreso')
    if (w.status != 'IN_PROGRESS') {
      iot.publish(`srv/${event.walkerId}/error`, {
        error: 'WORK_NOT_IN_PROGRESS'
      }, callback)
    } else {
      console.log('Trabajo en progreso. Finaliza trabajo')
      await dynamodb.call('update', updateParams_Works);
      console.log('validar si hay una ruta activa')
      const WalkRoutes_isActive = await dynamodb.call('query', queryParams_WalkRoutes_isActive);
      if (WalkRoutes_isActive.Items.length > 0){
        console.log('Existe ruta activa')
        const WalkRouteId = WalkRoutes_isActive.Items[0].id
        const wip = await dynamodb.call('query', queryParams_Works_WalkRoutes_InProgress(WalkRouteId));
        if (wip.Items.length == 0){
          console.log('No hay trabajos en progreso para la ruta, entonces se desactiva')
          await dynamodb.call('update', updateParams_WalkRoutes(WalkRouteId));
          const _walkRoute = await dynamodb.call('query', queryParams_WalkRoutes(WalkRouteId));
          iot.publish(`srv/${event.walkerId}/walkRoutes`, _walkRoute.Items, callback)
          iot.publish(`srv/${event.customerId}/walkRoutes`, _walkRoute.Items, callback)
          iot.publish(`srv/${event.walkerId}/action`, {action: 'STOP_WATCH_POSITION'}, callback)
        } else {
          console.log('Hay trabajos en progreso para la ruta')
        }
      } else {
        console.log('no hay una ruta activa...')
      }

      
      console.log('inserta registro de actividad')
      await dynamodb.call('put', putParams_WorkActivity);

      const _work = await dynamodb.call('query', queryParams_Works);

      iot.publish(`srv/${event.walkerId}/works`, _work.Items, callback)
      iot.publish(`srv/${event.customerId}/works`, _work.Items, callback)

    }

    callback(null, success(work));
   
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false, error: e }));
  }
};
