/*
  Copyright 2017-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.

  Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except
  in compliance with the License. A copy of the License is located at

      http://aws.amazon.com/apache2.0/

  or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
  specific language governing permissions and limitations under the License.
*/

import { v1 as uuid } from 'uuid';
import * as dynamodb from '../helpers/dynamodb';
import * as iot from '../helpers/aws-iot';
import { success, failure } from '../helpers/response';

export const main = async (event, context, callback) => {

  console.log(event)

  const queryParams_Works = {
    TableName: 'Works',
    KeyConditionExpression: "id = :id",
    ExpressionAttributeValues: {
      ":id": event.id
    }    
  };  

  
  const queryParams_Works_Walker_Rating = {
    TableName: 'Works',
    IndexName: 'walkerId_rating_ix',
    KeyConditionExpression: "walkerId = :walkerId and rating >= :rating",
    ExpressionAttributeValues: {
      ":walkerId": event.walkerId,
      ':rating': 0
    }    
  }; 

  const updateParams_Works = {
    TableName: 'Works',
    Key: { id : event.id },
    UpdateExpression: 'set #status = :closed , updatedat = :updatedat, rating = :rating',
    ExpressionAttributeNames: {'#status': 'status'},
    ExpressionAttributeValues: {
      ':closed' : 'CLOSED',
      ':updatedat' : event.updatedat,
      ':rating' : event.rating
    }
  }; 

  const updateParams_Users = (rates5,rates4,rates3,rates2,rates1,prom) => ({
    TableName: 'Users',
    Key: { identityId : event.walkerId },
    UpdateExpression: 'set rates5 = :rates5, rates4 = :rates4, rates3 = :rates3, rates2 = :rates2, rates1 = :rates1, prom = :prom',
    ExpressionAttributeValues: {
      ':rates5' : rates5,
      ':rates4' : rates4,
      ':rates3' : rates3,
      ':rates2' : rates2,
      ':rates1' : rates1,
      ':prom' : prom
    }
  }); 

  const queryParams_Users = {
    TableName: 'Users',
    KeyConditionExpression: "identityId = :id",
    ExpressionAttributeValues: {
      ":id": event.walkerId
    }    
  };

  const updateParams_WalkRoutes = (WalkRouteId) => ({
    TableName: 'WalkRoutes',
    Key: { id : WalkRouteId },
    UpdateExpression: 'set isActive = :false , finishedat = :finishedat',
    ExpressionAttributeValues: {
      ':false' : 'false',
      ':finishedat' : event.updatedat,
    }
  }); 

  const putParams_WorkActivity = {
    TableName: 'WorkActivity',
    Item: {
      id: uuid(),
      workId: event.id,
      createdat: event.updatedat,
      status: 'CLOSED'
    },
  } 


  try {
    const work = await dynamodb.call('query', queryParams_Works);
    const w = work.Items[0]
    console.log('Validar si el trabajo esta Finalizado')
    if (w.status != 'FINISHED') {
      iot.publish(`srv/${event.walkerId}/error`, {
        error: 'WORK_NOT_FINISHED'
      }, callback)
    } else {
      console.log('Trabajo finalizado. Inserta Calificacion y cierra trabajo')
      await dynamodb.call('update', updateParams_Works);
      
      console.log('inserta registro de actividad')
      await dynamodb.call('put', putParams_WorkActivity);

      console.log('Consulta trabajos calificados para estadisticas')
      const ratings = await dynamodb.call('query', queryParams_Works_Walker_Rating);
      const _5starts = ratings.Items.filter(e => e.rating == 5).length
      const _4starts = ratings.Items.filter(e => e.rating == 4).length
      const _3starts = ratings.Items.filter(e => e.rating == 3).length
      const _2starts = ratings.Items.filter(e => e.rating == 2).length
      const _1starts = ratings.Items.filter(e => e.rating == 1).length
      const prom_rate = ratings.Items.reduce((prev, curr, idx, array) => (idx == array.length-1) ? ((prev + curr.rating) / array.length) : (prev + curr.rating), 0).toFixed(1)
      
      await dynamodb.call('update', updateParams_Users(_5starts,_4starts,_3starts,_2starts,_1starts,prom_rate));
      const _user = await dynamodb.call('query', queryParams_Users);
      iot.publish(`srv/${event.customerId}/users`, _user.Items, callback)

      const _work = await dynamodb.call('query', queryParams_Works);
      iot.publish(`srv/${event.walkerId}/works`, _work.Items, callback)
      iot.publish(`srv/${event.customerId}/works`, _work.Items, callback)

    }

    callback(null, success(work));
   
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false, error: e }));
  }
};
