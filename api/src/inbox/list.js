/*
  Copyright 2017-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.

  Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except
  in compliance with the License. A copy of the License is located at

      http://aws.amazon.com/apache2.0/

  or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
  specific language governing permissions and limitations under the License.
*/

import { v4 as uuid } from 'uuid';
import * as dynamodb from '../helpers/dynamodb';
import * as iot from '../helpers/aws-iot';
import { success, failure } from '../helpers/response';
import { removeEmptyStringElements } from '../helpers/wipEmpty';
import { pushToArray } from '../helpers/pushToArray';
import { asyncForEach } from '../helpers/asyncForEach';


export const main = async (event, context, callback) => {

  const queryParams_Inbox = {
    TableName: 'Inbox',
    IndexName: 'identityId_ix',
    KeyConditionExpression: 'identityId = :identityId',
    ExpressionAttributeValues: {
      ':identityId': event.identityId,
    },
  };

  const queryParams_Works = (id) => ({
    TableName: 'Works',
    KeyConditionExpression: 'id = :id',
    ExpressionAttributeValues: {
      ':id': id,
    },
  });

  const queryParams_Users = (identityId) => ({
    TableName: 'Users',
    KeyConditionExpression: 'identityId = :identityId',
    ExpressionAttributeValues: {
      ':identityId': identityId,
    }
  });

  const queryParams_Pets = (identityId) => ({
    TableName: 'Pets',
    IndexName: 'identityId_ix',
    KeyConditionExpression: 'identityId = :identityId',
    ExpressionAttributeValues: {
      ':identityId': identityId,
    },
  });

  const queryParams_Locations = (identityId) => ({
    TableName: 'Locations',
    IndexName: 'identityId_ix',
    KeyConditionExpression: 'identityId = :identityId',
    ExpressionAttributeValues: {
      ':identityId': identityId,
    },
  });

  let listWorks = []
  let listUsers = []

  const TRX_ID = uuid();
  const TRX_SIZE = (event.profile == 'walker' ? 5 : 3)

  try {
    const Inbox = await dynamodb.call('query', queryParams_Inbox); //Consultar informacion de Inbox
    Inbox.Items.forEach(e => pushToArray(listWorks, e.workId)); //Listar Trabajos relacionados

    const w = await dynamodb.call('scan', {TableName: 'Works'});
    const Works = w.Items.filter(e => (listWorks.findIndex((i) => i === e.id) !== -1)) //Filtrar trabajos relacionados a enviar
    
    Works.forEach(e => {pushToArray(listUsers, event.profile == 'customer' ? e.walkerId : e.identityId)}); //Listar Usuarios relacionados
    console.log('USERS: ', listUsers)

    //Enviar informacion de Usuarios
    let Users = []
    let Pets = []
    let Locations = []
    
    const start = async () => {
      await asyncForEach(listUsers, async (e) => {
        const _User = await dynamodb.call('query', queryParams_Users(e));
        Users = Users.concat(_User.Items)

        if (event.profile == 'walker') {        
          const _Pets = await dynamodb.call('query', queryParams_Pets(e));
          Pets = Pets.concat(_Pets.Items) 
          
          const _Locations = await dynamodb.call('query', queryParams_Locations(e));
          Locations = Locations.concat(_Locations.Items) 
        }      
      });
      console.log('Done');
    }
    
    await start()

    let TRX_IX = 0

    iot.publish(`srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/${++TRX_IX}/users`, Users, callback)  

    if (event.profile == 'walker') {
      iot.publish(`srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/${++TRX_IX}/pets`, Pets, callback)
      iot.publish(`srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/${++TRX_IX}/locations`, Locations, callback)
    }

    //Enviar informacion de Trabajos
    iot.publish(`srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/${++TRX_IX}/works`, Works, callback)
    console.log('WORKS: ', Works)

    //Enviar informacion de Inbox
    iot.publish(`srv/${event.identityId}/TRX/${TRX_ID}/${TRX_SIZE}/${++TRX_IX}/inbox`, Inbox.Items, callback)
    console.log('INBOX: ', Inbox)

    callback(null, success(Inbox.Items));
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false, error: e }));
  }
};
