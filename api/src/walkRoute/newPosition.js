/*
  Copyright 2017-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.

  Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except
  in compliance with the License. A copy of the License is located at

      http://aws.amazon.com/apache2.0/

  or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
  specific language governing permissions and limitations under the License.
*/

import { v1 as uuid } from 'uuid';
import * as dynamodb from '../helpers/dynamodb';
import * as iot from '../helpers/aws-iot';
import { success, failure } from '../helpers/response';

export const main = async (event, context, callback) => {

  console.log(event)

  const queryParams_GeoLocations = (WalkRoutePositionId) => ({
    TableName: 'GeoLocations',
    KeyConditionExpression: "id = :WalkRoutePositionId",
    ExpressionAttributeValues: {
      ":WalkRoutePositionId": WalkRoutePositionId
    }    
  })

  const queryParams_Works = {
    TableName: 'Works',
    IndexName: 'walkRouteId_ix',
    KeyConditionExpression: "walkRouteId = :walkRouteId",
    ExpressionAttributeValues: {
      ":walkRouteId": event.walkRouteId
    }    
  };  

  const queryParams_WalkRoutes = {
    TableName: 'WalkRoutes',
    KeyConditionExpression: "id = :id",
    ExpressionAttributeValues: {
      ":id": event.walkRouteId
    }    
  }; 

  const putParams_GeoLocations = (WalkRoutePositionId) => ({
    TableName: 'GeoLocations',
    Item: {
      id: WalkRoutePositionId,
      walkRouteId: event.walkRouteId,
      createdat: event.createdat,
      walkerId: event.walkerId,
      latitude: event.latitude,
      longitude: event.longitude
    },
  })

  let WalkRoutePositionId = uuid()

  try {
    const walkRoute = await dynamodb.call('query', queryParams_WalkRoutes);
    const wr = walkRoute.Items[0]
    if(wr.isActive == 'false') {
      iot.publish(`srv/${event.walkerId}/error`, {
        error: 'WALKROUTE_NOT_ACTIVE'
      }, callback)
    } else {

      await dynamodb.call('put', putParams_GeoLocations(WalkRoutePositionId));
      
      const geoLocations = await dynamodb.call('query', queryParams_GeoLocations(WalkRoutePositionId));
      const works = await dynamodb.call('query', queryParams_Works);


      iot.publish(`srv/${event.walkerId}/geoLocations`, geoLocations.Items, callback)

      works.Items.forEach(e => {
        iot.publish(`srv/${e.identityId}/geoLocations`, geoLocations.Items, callback)
      })

    }

    callback(null, success(walkRoute));
   
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false, error: e }));
  }
};
