export const pushToArray = (arr, obj) => {
    const index = arr.findIndex((e) => e === obj);

    if (index === -1) {
        if (obj != '' && obj != null) arr.push(obj);
    } else {
        arr[index] = obj;
    }
}