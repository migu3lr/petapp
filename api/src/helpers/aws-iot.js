/*
  Copyright 2017-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.

  Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except
  in compliance with the License. A copy of the License is located at

      http://aws.amazon.com/apache2.0/

  or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
  specific language governing permissions and limitations under the License.
*/

import { Iot, IotData } from 'aws-sdk';
import { success, failure } from '../helpers/response';
import config from '../../../src/config'

export const attachPrincipalPolicy = (policyName, principal) => {
  const iot = new Iot();
  const params = { policyName, principal };
  return iot.attachPrincipalPolicy(params).promise();
};

export const createPolicy = (policyDocument, policyName) => {
  const iot = new Iot();
  const params = { policyDocument, policyName };
  return iot.createPolicy(params).promise();
};

const iotdata = new IotData({ endpoint: config.awsIotHost })

export const publish = (topic, payload, callback) => {
  var params = {
    topic,
    payload: JSON.stringify(payload),
    qos: 0
    };

  iotdata.publish(params, function(err, data){
    if(err){
        console.log("Error occured : ",err);
        callback(null, failure({ status: false, error: err }));
    }
    else{
        console.log("Iot Publish OK",params);
    }
  });
}
